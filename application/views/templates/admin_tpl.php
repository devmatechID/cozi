<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>Mawas Ozon | Monitoring dan Pengawasan Ozon</title>
	
	<link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>css/sb-admin.css" rel="stylesheet">
	
	<link href="<?php echo base_url(); ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	
	<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>css/style-xs.css" rel="stylesheet">
	
	<script src="<?php echo base_url(); ?>js/jquery-1.12.4.min.js"></script>
	<script src="<?php echo base_url(); ?>js/jquery.number.min.js"></script>
	<script src="<?php echo base_url(); ?>js/autoNumeric-min.js"></script>
	<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
	
	<link href="<?php echo base_url(); ?>js/select2/select2.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>js/select2/select2.hack.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>js/select2/select2.full.min.js"></script>
	
	<link href="<?php echo base_url(); ?>js/datepicker/bootstrap-datepicker.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>js/datepicker/bootstrap-datepicker.min.js"></script>
	
	<script>var base_url = '<?php echo base_url(); ?>';</script>
	
	<script>
	function currency_to_num(str)
	{
		if (str == '') return 0;
		str = str.replace(/\./g, '');
		return parseInt(str);
	}
	
	
	$().ready(function() {
		
		function check_required()
		{
			if ($(this).val() == '') $(this).closest('.form-group').addClass('has-error');
			else $(this).closest('.form-group').removeClass('has-error');
		}
		
		$('[type=text], [type=email], [type=password]').attr('autocomplete', 'off');
		$('.datepicker').datepicker({ format: 'yyyy-mm-dd', autoclose: true });
		$('.required').blur(check_required);
		$('select.required').change(check_required);
		
		$('select').select2();
		$('select').on('change', function() {
			$(this).select2('open').select2('close');
		});
		
		$('.number').autoNumeric('init', {
			aSep: '.', aDec: ',', aSign: '', mDec: '0'
		});
		
		$('.decimal').autoNumeric('init', {
			aSep: '.', aDec: ',', aSign: ''
		});
		
		$('.validate-form').submit(function() {
			var has_error = false;
			$.each($('.required'), function(idx, obj) {
				if ($(obj).val() == '') {
					$(obj).closest('.form-group').addClass('has-error');
					has_error = true;
				}
			});
			if (has_error) alert('Mohon lengkapi form!');
			return !has_error;
		});
	});
	</script>
</head>
<body>
	<div id="wrapper">
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo site_url('/dasbor') ?>"><img src="<?php echo base_url(); ?>img/mawas-logo.png" width="100px"></a>
			</div>
			<ul class="nav navbar-right top-nav">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> &nbsp; <?php echo user_session('nama'); ?> <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"><i class="fa fa-fw fa-user"></i> Profil</a></li>
						<li><a href="<?php echo site_url('/pengguna/auth/password'); ?>"><i class="fa fa-fw fa-key"></i> Ubah Password</a></li>
						<li class="divider"></li>
						<li><a href="<?php echo site_url('/pengguna/auth/logout'); ?>"><i class="fa fa-fw fa-power-off"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav side-nav">
					<li><a href="<?php echo site_url('/dasbor'); ?>"><i class="fa fa-fw fa-dashboard"></i> Beranda</a></li>
					<?php if ( ! (user_session('tingkatan') == '5')): ?>
					<li>
						<a href="#" data-toggle="collapse" data-target="#side-menu-perusahaan">
							<i class="fa fa-fw fa-database"></i> Data Perusahaan <i class="fa fa-fw fa-caret-down"></i>
						</a>
						<ul id="side-menu-perusahaan" class="collapse">
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'supplier') ): ?>
							<li><a href="<?php echo site_url('/perusahaan/supplier'); ?>">Foam - System House</a></li>
						<?php endif; ?>	
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'besar') ): ?>
							<li><a href="<?php echo site_url('/perusahaan/besar'); ?>">Foam - Perusahaan Besar</a></li>
						<?php endif; ?>	
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'ukm') ): ?>
							<li><a href="<?php echo site_url('/perusahaan/ukm'); ?>">Foam - UKM</a></li>
						<?php endif; ?>	
						<?php if ( ! (user_session('tingkatan') == '4') ): ?>
							<li><a href="<?php echo site_url('/perusahaan/refrigerasi'); ?>">Manufaktur Refrigerasi</a></li>
							<li><a href="<?php echo site_url('/perusahaan/ac'); ?>">Manufaktur AC</a></li>
						<?php endif; ?>	
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'institusi') ): ?>	
							<li><a href="<?php echo site_url('/perusahaan/institusi'); ?>">Institusi Pendidikan</a></li>
						<?php endif; ?>	
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'bengkel') ): ?>
							<li><a href="<?php echo site_url('/perusahaan/bengkel'); ?>">Bengkel / Workshop</a></li>
						<?php endif; ?>		
						</ul>
					</li>
					<?php endif; ?>	
					<?php if ( ! (user_session('tingkatan') == '5')): ?>
					<li>
						<a href="#" data-toggle="collapse" data-target="#side-menu-hpmp">
							<i class="fa fa-fw fa-files-o"></i> HPMP <i class="fa fa-fw fa-caret-down"></i>
						</a>
						<ul id="side-menu-hpmp" class="collapse">
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'supplier') ): ?>
							<li><a href="<?php echo site_url('/hpmp/supplier'); ?>">Foam - System House</a></li>
						<?php endif; ?>	
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'besar') ): ?>
							<li><a href="<?php echo site_url('/hpmp/besar'); ?>">Foam - Perusahaan Besar</a></li>
						<?php endif; ?>
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'ukm') ): ?>
							<li><a href=<?php echo site_url('/hpmp/ukm'); ?>>Foam - UKM</a></li>
						<?php endif; ?>		
							<!-- <li><a href="#">Manufaktur Refrigerasi</a></li> -->
							<!-- <li><a href="#">Manufaktur AC</a></li> -->
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'institusi') ): ?>
							<li><a href="<?php echo site_url('/hpmp/institusi'); ?>">Institusi Pendidikan</a></li>
						<?php endif; ?>	
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'bengkel') ): ?>
							<li><a href="<?php echo site_url('/hpmp/bengkel'); ?>">Bengkel / Workshop</a></li>
						<?php endif; ?>	
						</ul>
					</li>
					<?php endif; ?>	
					<li><a href="<?php echo site_url('/laporan/bengkel'); ?>"><i class="fa fa-fw fa-tasks"></i> Lap. Harian Bengkel</a></li>
					<li><a href="<?php echo site_url('/laporan/bengkel_bulanan'); ?>"><i class="fa fa-fw fa-calendar"></i> Lap. Bulanan Bengkel</a></li>
					<?php if ( ! (user_session('tingkatan') == '5')): ?>
					<li>
						<a href="#" data-toggle="collapse" data-target="#side-menu-pelaporan">
							<i class="fa fa-fw fa-archive"></i> Pelaporan <i class="fa fa-fw fa-caret-down"></i>
						</a>
						<ul id="side-menu-pelaporan" class="collapse">
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'supplier') ): ?>
							<li><a href="<?php echo site_url('/pelaporan/supplier'); ?>">Foam - System House</a></li>
						<?php endif; ?>	
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'besar') ): ?>
							<li><a href="<?php echo site_url('/pelaporan/besar'); ?>">Foam - Perusahaan Besar</a></li>
						<?php endif; ?>
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'ukm') ): ?>
							<li><a href=<?php echo site_url('/pelaporan/ukm'); ?>>Foam - UKM</a></li>
						<?php endif; ?>		
							<!-- <li><a href="#">Manufaktur Refrigerasi</a></li> -->
							<!-- <li><a href="#">Manufaktur AC</a></li> -->
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'institusi') ): ?>
							<li><a href="<?php echo site_url('/pelaporan/institusi'); ?>">Institusi Pendidikan</a></li>
						<?php endif; ?>	
						<?php if ( ! (user_session('tingkatan') == '4') || (user_session('grup_pengguna') == 'bengkel') ): ?>
							<li><a href="<?php echo site_url('/pelaporan/bengkel'); ?>">Bengkel / Workshop</a></li>
						<?php endif; ?>	
						</ul>
					</li>
					<?php endif; ?>	

					<?php if ( ! (user_session('tingkatan') == '5') && ! (user_session('tingkatan') == '4')): ?>
					<li>
						<a href="#" data-toggle="collapse" data-target="#side-menu-rekap">
							<i class="fa fa-fw fa-rss"></i> Rekap <i class="fa fa-fw fa-caret-down"></i>
						</a>
						<ul id="side-menu-rekap" class="collapse">
						<li><a href="<?php echo site_url('/rekap/freon'); ?>">Penggunaan Freon</a></li>
						<li><a href="<?php echo site_url('/rekap/hcfc'); ?>">Penggunaan HCFC</a></li>
						</ul>
					</li>
					<?php endif; ?>				

					<?php if ( ! (user_session('tingkatan') == '5')): ?>
					<li><a href="<?php echo site_url('/konten/monitoring'); ?>"><i class="fa fa-fw fa-desktop"></i> Monitoring</a></li>
					<?php endif; ?>
					<li>
						<a href="#" data-toggle="collapse" data-target="#side-menu-konten">
							<i class="fa fa-fw fa-file-text-o"></i> Konten <i class="fa fa-fw fa-caret-down"></i>
						</a>
						<ul id="side-menu-konten" class="collapse">
							<li><a href="#">Tutorial</a></li>
							<li><a href="<?php echo site_url('/konten/tips'); ?>">Tips &amp; Trik</a></li>
						</ul>
					</li>
					<li><a href="<?php echo site_url('/konten/sosialisasi'); ?>"><i class="fa fa-fw fa-bullhorn"></i> Sosialisasi</a></li>
					<li><a href="<?php echo site_url('/konten/meeting')?>"><i class="fa fa-fw fa-users"></i> Meeting &amp; Konferensi</a></li>
					<li><a href="<?php echo site_url('/konten/pengaduan')?>"><i class="fa fa-fw fa-exclamation-triangle"></i> Pengaduan</a></li>
					<li><a href="<?php echo site_url('/konten/balai')?>"><i class="fa fa-fw fa-bank"></i> Balai</a></li>
					<?php if ( (user_session('tingkatan') == '1')): ?>
					<li><a href="<?php echo site_url('/konten/kategori'); ?>"><i class="fa fa-fw fa-cog"></i> Kategori Pengaduan</a></li>
					<li><a href="<?php echo site_url('/pengguna'); ?>"><i class="fa fa-fw fa-users"></i> Manajemen Pengguna</a></li>
					<?php endif; ?>
					
					<?php $penanggung_jawab = $this->db->get_where('bengkel', array('id' => user_session('id_organisasi')))->row('penanggung_jawab'); ?>
					<?php $nama_user = $this->db->get_where('pengguna', array('id' => user_session('id')))->row('nama'); ?>
					<?php if ( user_session('tingkatan') == '4' && ($nama_user == $penanggung_jawab) ): ?>
					<li><a href="<?php echo site_url('/pengguna/bengkel'); ?>"><i class="fa fa-fw fa-users"></i> Manajemen Pengguna</a></li>
					<?php endif; ?>
					
					<li><a href="<?php echo site_url('/pengguna/auth/logout'); ?>"><i class="fa fa-fw fa-power-off"></i> Logout</a></li>
				</ul>
			</div>
		</nav>
		<div id="page-wrapper">
			<?php $this->load->view($content); ?>
		</div>
	</div>
</body>
</html>