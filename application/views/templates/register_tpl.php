<!DOCTYPE HTML>
<html id="signup">
<head>
	<meta charset="utf-8">
	<title>Mawas Ozon | Pendaftaran</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
	
	<script src="<?php echo base_url(); ?>js/jquery-1.12.4.min.js"></script>
	<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
	
	<link href="<?php echo base_url(); ?>js/select2/select2.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>js/select2/select2.hack.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>js/select2/select2.full.min.js"></script>
	
	<script>
	$().ready(function() {
		$('[type=text], [type=email], [type=password]').attr('autocomplete', 'off');
		
		$('select').select2();
		$('select').on('change', function() {
			$(this).select2('open').select2('close');
		});
		
		$('form').submit(function(e) {
			e.preventDefault();
			
			$('form .form-control').attr('readonly', true);
			$('button[type=submit]').attr('disabled', true);
			$('button[type=submit]').text('Menyimpan data...');
			
			$.post (
				'<?php echo $action_url; ?>'
				, $('form').serialize()
				, function(response) {
					if ( ! alert('Pendaftaran berhasil! User ID dan Password kami kirimkan via SMS. Terima kasih.')) {
						window.location = '<?php echo site_url('/login'); ?>';
					}
				}
			);
		});
	});
	</script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div id="signup-container" class="col-sm-10 col-xs-12">
				<h1 align="center"><img src="<?php echo base_url(); ?>img/mawas-logo.png" class="img-responsive"></h1>
				<div class="panel panel-default">
					<?php $this->load->view($content); ?>
					<div class="panel-footer">
						<a href="<?php echo site_url('/login'); ?>">Kembali ke Halaman Login</a>
					</div>
				</div>
				<div id="signup-footer">
					2016 &copy; Kementerian Lingkungan Hidup dan Kehutanan</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>