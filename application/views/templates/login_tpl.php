<!DOCTYPE HTML>
<html id="login">
<head>
	<meta charset="utf-8">
	<title>Mawas Ozon | Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>js/jquery-1.12.4.min.js"></script>
	<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
	<script>
	$().ready(function() {
		$('[type=text], [type=email], [type=password]').attr('autocomplete', 'off');
	});
	</script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div id="login-container" class="col-md-4 col-xs-12">
				<h1><img src="<?php echo base_url(); ?>img/mawas-logo.png" class="img-responsive"></h1>
				<div class="panel panel-default">
					<div class="panel-body">
						<?php if ($this->session->flashdata('login_status') == 'failed'): ?>
						<div id="login-err">Login gagal, periksa kembali User ID dan password Anda.</div>
						<?php endif; ?>
						<form method="post" role="form" action="<?php echo site_url('/pengguna/auth/login'); ?>">
							<div class="form-group">
								<label>User ID</label>
								<input type="text" class="form-control" name="user_id">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" name="password">
							</div>
							<button type="submit" class="btn-lg btn-default">Login Sekarang</button>
						</form>
					</div>
					<div class="panel-footer">
						<a href="#">Lupa Password?</a>
					</div>
				</div>
				<div id="register-link">
					Belum menjadi anggota? &nbsp;
					<a href="#" class="btn btn-success" role="button" data-toggle="modal" data-target="#modal-options">DAFTAR</a>
				</div>
				<div id="login-footer">
					2016 &copy; COZI oleh Kementerian Lingkungan Hidup &amp; Kehutanan
				</div>
			</div>
		</div>
	</div>
</body>
</html>

<div id="modal-options" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Pilih Organisasi</h4>
			</div>
			<div class="modal-body">
				<ul id="register-options">
					<li><a href="<?php echo site_url('/pendaftaran/besar'); ?>"><img src="<?php echo base_url('/img/industry.png'); ?>"> Foam - Perusahaan Besar</a></li>
					<li><a href="<?php echo site_url('/pendaftaran/ukm'); ?>"><img src="<?php echo base_url('/img/smb.png'); ?>"> Foam - UKM</a></li>
					<li><a href="<?php echo site_url('/pendaftaran/institusi'); ?>"><img src="<?php echo base_url('/img/education.png'); ?>"> Institusi Pendidikan</a></li>
					<li><a href="<?php echo site_url('/pendaftaran/bengkel'); ?>"><img src="<?php echo base_url('/img/workshop.png'); ?>"> Bengkel / Workshop</a></li>
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batalkan</button>
			</div>
		</div>
	</div>
</div>