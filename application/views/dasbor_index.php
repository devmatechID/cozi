<script src="<?php echo base_url(); ?>js/pBar/jquery.pBar.min.js"></script>

<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Dasbor Nasional Tahun 2016</h1>
</div>
<div id="main-container" class="container-fluid">
	<div class="col-sm-3">
		<div class="dashboard-card" id="card-royalblue">
			<div class="col-xs-4" align="center">
				<span class="glyphicon glyphicon-folder-open"></span>
			</div>
			<div class="col-xs-8" align="center">
				<?php echo $num_foam; ?><br><div class="dashboard-small">Perusahaan<br>Busa / Foam</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="dashboard-card" id="card-green">
			<div class="col-xs-4" align="center">
				<span class="glyphicon glyphicon-hdd"></span>
			</div>
			<div class="col-xs-8" align="center">
				<?php echo $num_manufaktur; ?><br><div class="dashboard-small">Manufaktur<br>Refrigerasi &amp; AC</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="dashboard-card" id="card-crimson">
			<div class="col-xs-4" align="center">
				<span class="glyphicon glyphicon-education"></span>
			</div>
			<div class="col-xs-8" align="center">
				<?php echo $num_institusi; ?><br><div class="dashboard-small">Institusi<br>Pendidikan</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="dashboard-card" id="card-orange">
			<div class="col-xs-4" align="center">
				<span class="glyphicon glyphicon-cog"></span>
			</div>
			<div class="col-xs-8" align="center">
				<?php echo $num_bengkel; ?><br><div class="dashboard-small">Bengkel /<br>Workshop</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="clearfix"></div>
	<br>
	<div class="col-sm-12">
	<div class="table-responsive">
		<table class="table table-profile table-striped table-condensed">
			<thead>
				<tr>
					<th>No.</th>
					<th>Provinsi</th>
					<th>Lap. Masuk</th>
					<th>Progress</th>
					<th>System House (Supplier)</th>
					<th>Perusahaan Besar</th>
					<th>UKM</th>
					<th>Bengkel</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$no=0;
					foreach ($propinsi->result() as $p): 
					$no++;
				?>
				<?php $progress = mt_rand(1, 100); ?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><?php echo $p->nama; ?></td>
					<td align="center"><?php echo $progress; ?>%</td>
					<td><div class="pBar" data-from="1" data-to="<?php echo $progress; ?>" data-color="<?php echo progress_color($progress); ?>"></div></td>
					<td align="center"><?php echo $p->supplier; ?></td>
					<td align="center"><?php echo $p->besar; ?></td>
					<td align="center"><?php echo $p->ukm; ?></td>
					<td align="center"><?php echo $p->beng; ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	</div>
</div>