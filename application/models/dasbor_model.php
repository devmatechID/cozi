<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dasbor_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
	}
	
	
	public function num_foam()
	{
		$this->db->where('deleted_at IS NULL');
		$this->db->from('besar');
		$num_besar = $this->db->count_all_results();
		
		$this->db->where('deleted_at IS NULL');
		$this->db->from('ukm');
		$num_ukm = $this->db->count_all_results();
		
		return $num_besar + $num_ukm;
	}

	public function num_manufaktur()
	{
		$this->db->where('deleted_at IS NULL');
		$this->db->from('refrigerasi');
		$num_refrigerasi = $this->db->count_all_results();
		
		$this->db->where('deleted_at IS NULL');
		$this->db->from('ac');
		$num_ac = $this->db->count_all_results();
		
		return $num_refrigerasi + $num_ac;
	}
	
	
	public function num_institusi()
	{
		$this->db->where('deleted_at IS NULL');
		$this->db->from('institusi');
		return $this->db->count_all_results();
	}
	
	
	public function num_bengkel()
	{
		$this->db->where('deleted_at IS NULL');
		$this->db->from('bengkel');
		return $this->db->count_all_results();
	}
}
/* End of file dasbor_model.php */
/* Location: ./application/models/dasbor_model.php */