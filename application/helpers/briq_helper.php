<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function briq_password($raw_password)
{
	return md5('BRIQ-'.$raw_password.'-*123#');
}

function user_session($field)
{
	$CI =& get_instance();
	$user = $CI->session->userdata('pengguna');
	return empty($user) ? null : $user->$field;
}

function options($src, $id, $ref_val, $text_field)
{
	$options = '';
	foreach ($src->result() as $row) {
		$opt_value	= $row->$id;
		$text_value	= $row->$text_field;
		
		if ($row->$id == $ref_val) {
			$options .= '<option value="'.$opt_value.'" selected>'.$text_value.'</option>';
		}
		else {
			$options .= '<option value="'.$opt_value.'">'.$text_value.'</option>';
		}
	}
	return $options;
}

function options_special($src, $id, $org, $ref_val, $text_field)
{
	$options = '';
	foreach ($src->result() as $row) {
		$opt_value	= $row->$id.'_'.$row->$org;
		$text_value	= $row->$text_field;
		
		if ($row->$id.'_'.$row->$org == $ref_val) {
			$options .= '<option value="'.$opt_value.'" selected>'.$text_value.'</option>';
		}
		else {
			$options .= '<option value="'.$opt_value.'">'.$text_value.'</option>';
		}
	}
	return $options;
}

function validate_form($names)
{
	$CI =& get_instance();
	
	foreach ($names as $name) {
		if ($CI->input->post($name) == '') {
			$CI->session->set_flashdata('form_status', 'invalid');
			redirect($CI->agent->referrer());
		}
	}
}

function getPropinsi($prop) {
    $CI =& get_instance();
    $ret = "";
    foreach( explode(",", $prop) AS $id_p ) {
        $db_p = $CI->db->get_where('propinsi', "deleted_at IS NULL AND id = $id_p");
        if( $id_p ) {
            $ret .= $db_p->row('nama') . ", ";
        }
    }
    
    return $ret;
}

function form_data($names)
{
	$CI =& get_instance();
	
	foreach ($names as $name) {
		$words = explode('_', $name);
		$prefix = $words[0];
		
		if ($prefix == 'num') {
			$name = substr($name, 4);
			$data[$name] = to_number(trim($CI->input->post($name)));
		}
		else if ($prefix == 'upper') {
			$name = substr($name, 6);
			$data[$name] = strtoupper(trim($CI->input->post($name)));
		}
		else if ($prefix == 'lower') {
			$name = substr($name, 6);
			$data[$name] = strtolower(trim($CI->input->post($name)));
		}
		else if ($prefix == 'tgl') {
			$tgl = trim($CI->input->post($name));
			$data[$name] = ($tgl == '') ? NULL : $tgl;
		}
		else {
			$data[$name] = trim($CI->input->post($name));
		}
	}
	
	return $data;
}

function date_id($date)
{
	if ($date == '') return '';
	$date_arr = explode('-', $date);
	return $date_arr[2].' '.get_month($date_arr[1]).' '.$date_arr[0];
}

function ganti_format_tgl($tgl = "")
{
	$tanggal = explode("-", $tgl);
	$tgl = $tanggal[2]."-".$tanggal[1]."-".$tanggal[0];
	return $tgl;
}

function strip_dot($text)
{
	return str_replace('.', '', $text);
}

function strip_comma($text)
{
	return str_replace(',', '', $text);
}

function to_number($text)
{
	return str_replace(',', '.', str_replace('.', '', $text));
}

function currency($number)
{
	return number_format($number, 0, ',', '.');
}

function decimal($number)
{
	return number_format($number, 2, ',', '.');
}

function percent($number)
{
	return decimal($number).' %';
}

function db_insert($table, $data)
{
	$CI =& get_instance();
	$data['created_by'] = user_session('id');
	return $CI->db->insert($table, $data);
}

function db_insert_batch($table, $data_arr)
{
	$CI =& get_instance();
	
	foreach ($data_arr as $key => $data) {
		$data_arr[$key]['created_by'] = user_session('id');
	}
	
	return $CI->db->insert_batch($table, $data_arr);
}

function db_update($table, $data, $where)
{
	$CI =& get_instance();
	$data['updated_at'] = date('Y-m-d H:i:s');
	$data['updated_by'] = user_session('id');
	return $CI->db->update($table, $data, $where);
}

function db_delete($table, $where)
{
	$CI =& get_instance();
	$data['deleted_at'] = date('Y-m-d H:i:s');
	$data['deleted_by'] = user_session('id');
	return $CI->db->update($table, $data, $where);
}

function check_token()
{
	$CI =& get_instance();
	
	$mawas_token = $CI->config->item('mawas_token');
	
	if ($CI->input->post('mawas_token') == $mawas_token) {
		return true;
	}
	else {
		header('Content-type:application/json', '', 401);
		echo json_encode(array('message' => 'Token Mawas tidak valid'));
		return false;
	}
}

function initial_pwd()
{
	
	return mt_rand(1,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9);
}

function sms($no_hp, $pesan)
{
	$userkey = 'vsch74';
	$passkey = 'supercozi*123#';

	$url = 'https://reguler.zenziva.net/apps/smsapi.php';
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$no_hp.'&pesan='.urlencode($pesan));
	curl_setopt($curl, CURLOPT_HEADER, 0);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_TIMEOUT,30);
	curl_setopt($curl, CURLOPT_POST, 1);

	$results = curl_exec($curl);
	curl_close($curl);
}

function sms_register($no_hp, $user_id, $password)
{
	$pesan =
		"Mawas Ozon App\n".
		"User ID: $user_id\n".
		"Password: $password\n\n";
	
	sms($no_hp, $pesan);
}

function location($point)
{
	$point = str_replace(' ', '', $point);
	$lat_long = explode(',', $point);
	
	return array (
		'lat' => $lat_long[0],
		'long' => $lat_long[1],
	);
}

function level_pengguna($tingkatan)
{
	switch ($tingkatan) {
		case 1: return 'NASIONAL';
		case 2: return 'PROPINSI';
		case 3: return 'KAB. / KOTA';
		case 4: return 'PERUSAHAAN';
		case 5: return 'PUBLIK';
	}
}

function to_upper($string)
{
	$string=strtoupper($string);
	return $string;
}

function jenis_usaha($jenis)
{
	switch ($jenis) {
		case 1: return 'JASA SERVICE AC PANGGILAN';
		case 2: return 'INSTALLER';
		case 3: return 'SERVICE COLD STORAGE';
		case 4: return 'SERVICE MANDIRI (GEDUNG / HOTEL)';
	}
}

function jenis_kegiatan($jenis)
{
	switch ($jenis) {
		case 0: return 'Pelatihan Saja';
		case 1: return 'Pelatihan & Sertifikasi';
	}
}

function status_pengaduan($jenis)
{
	switch ($jenis) {
		case 0: $status = 'PENDING'; $color ='Red'; break;
		case 1: $status = 'SOLVED'; $color ='Blue'; break;
	}
	return '<font color="'.$color.'">'.$status.'</font>';
}

function aktif_pasif($jenis)
{
	switch ($jenis) {
		case 0: return 'NON AKTIF';
		case 1: return 'AKTIF';
	}
}

function jenis_produk($jenis)
{
	switch ($jenis) {
		case 'hcfc': return 'HCFC';
		case 'preblended': return 'Preblended';
	}
}

function progress_color($progress)
{
	if ($progress == 0) return 'Crimson';
	if ($progress > 0 && $progress <= 25) return 'IndianRed';
	if ($progress > 25 && $progress <= 50) return 'Orange';
	if ($progress > 50 && $progress <= 65) return 'Gold';
	if ($progress > 65 && $progress <= 80) return 'RoyalBlue';
	if ($progress > 80 && $progress < 100) return 'MediumAquaMarine';
	if ($progress == 100) return 'ForestGreen';
}

function exif($image_path)
{
	// echo file_exists($image_path);
	// if(file_exists($image_path)){
		$info = exif_read_data($image_path, TRUE);
	
		if (isset($info['GPSLatitude']) && isset($info['GPSLongitude']) && isset($info['GPSLatitudeRef']) && isset($info['GPSLongitudeRef']) && in_array($info['GPSLatitudeRef'], array('E', 'W', 'N', 'S')) && in_array($info['GPSLongitudeRef'], array('E', 'W', 'N', 'S'))) {
			
			$GPSLatitudeRef = strtolower(trim($info['GPSLatitudeRef']));
			$GPSLongitudeRef = strtolower(trim($info['GPSLongitudeRef']));
			
			$lat_degrees_a = explode('/', $info['GPSLatitude'][0]);
			$lat_minutes_a = explode('/', $info['GPSLatitude'][1]);
			$lat_seconds_a = explode('/', $info['GPSLatitude'][2]);
			$lng_degrees_a = explode('/', $info['GPSLongitude'][0]);
			$lng_minutes_a = explode('/', $info['GPSLongitude'][1]);
			$lng_seconds_a = explode('/', $info['GPSLongitude'][2]);
			
			$lat_degrees = $lat_degrees_a[0] / $lat_degrees_a[1];
			$lat_minutes = $lat_minutes_a[0] / $lat_minutes_a[1];
			$lat_seconds = $lat_seconds_a[0] / $lat_seconds_a[1];
			$lng_degrees = $lng_degrees_a[0] / $lng_degrees_a[1];
			$lng_minutes = $lng_minutes_a[0] / $lng_minutes_a[1];
			$lng_seconds = $lng_seconds_a[0] / $lng_seconds_a[1];
			
			$lat = (float) $lat_degrees + ((($lat_minutes * 60) + ($lat_seconds)) / 3600);
			$lng = (float) $lng_degrees + ((($lng_minutes * 60) + ($lng_seconds)) / 3600);
			
			$GPSLatitudeRef == 's' ? $lat *= -1 : '';
			$GPSLongitudeRef == 'w' ? $lng *= -1 : '';
			
			return $lat.','.$lng;
		}
	// }
	
	return NULL;
}

function cek_lokasi($lokasi)
{
	if ($lokasi == NULL OR $lokasi == '' OR $lokasi == '0') return '-';
	else return '<font color="RoyalBlue"><i class="glyphicon glyphicon-ok"></i></font>';
}

function cek_tuk($tuk)
{
	if ($tuk == NULL OR $tuk == '' OR $tuk == '0') return '-';
	else return '<font color="RoyalBlue"><i class="glyphicon glyphicon-ok"></i></font>';
}


/* End of file briq_helper.php */
/* Location: ./application/helpers/briq_helper.php */