<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Institusi extends MX_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->page->use_directory();
		$this->load->model('institusi_model');
	}
	
	
	public function index($q_encoded = 'x')
	{
		$this->institusi_model->set_filter($q_encoded);
		
		$this->grid->init(array (
			'base_url' => $this->page->base_url("/index/$q_encoded"),
			'act_url' => $this->page->base_url(),
			'uri_segment' => 5,
			'items'	=> array (
				'created_at' => array('text' => 'Waktu Input'),
				'tanggal' => array('text' => 'Tanggal'),
				'jenis' => array('text' => 'Jenis Kegiatan'),
				'institusi' => array('text' => 'Nama Institusi'),
				'pengguna' => array('text' => 'Pengguna Input'),
				'peserta' => array('text' => 'Jumlah Peserta'),
				'peserta_lulus' => array('text' => 'Jumlah Peserta Lulus'),
				'sertifikasi' => array('text' => 'Sertifikasi'),
			),
			'num_rows' => $this->institusi_model->num_rows(),
			'item' => 'institusi',
			'order' => 'desc',
			'warning' => 'institusi',
			'checkbox' => FALSE,
		));
		
		$this->institusi_model->set_grid_params($this->grid->params());
		$this->grid->source($this->institusi_model->get());
		
		$this->page->view('institusi_index', array (
			'add_url' => $this->page->base_url('/tambah'),
			'insert_url' => $this->page->base_url('/insert'),
			'show_url' => $this->page->base_url('/show'),
			'filter' => (object) $this->institusi_model->filter,
			'keyword' => $this->institusi_model->keyword,
			'grid' => $this->grid->draw(),
			'pagelink' => $this->grid->page_link(),
		));
	}
	
	
	public function show()
	{
		$q_encoded = base64_encode(json_encode($this->input->post()));
		redirect($this->page->base_url("/index/$q_encoded"));
	}
	
	
	public function form($action = 'insert', $id = '')
	{
		if ($this->agent->referrer() == '') redirect($this->page->base_url());
		
		$this->page->view('institusi_form', array (
			'action_url' => $this->page->base_url("/$action/$id"),
			'redirect' => $this->agent->referrer(),
			'data' => $this->institusi_model->by_id($id),
		));
	}
	
	
	public function tambah()
	{
		$this->form();
	}
	
	
	public function ubah($id)
	{
		$this->form('update', $id);
	}
	
	
	public function form_data()
	{
		$names = array('id_institusi', 'peserta', 'peserta_lulus', 'sertifikasi','jenis_kegiatan');
		return form_data($names);
	}
	
	
	public function insert()
	{
		$tgl = $this->input->post("tgl_pelatihan");
		$data = $this->form_data();
		$data['tgl_pelatihan'] = $tgl;
		db_insert('institusi_laporan', $data);
		redirect($this->input->post('redirect'));
	}
	
	
	public function update($id)
	{
		$tgl = $this->input->post("tgl_pelatihan");
		$data = $this->form_data();
		$data['tgl_pelatihan'] = $tgl;
		db_update('institusi_laporan', $data, array('id' => $id));
		redirect($this->input->post('redirect'));
	}
	
	
	public function profil($id)
	{
		$this->page->view('institusi_profil', array (
			'data' => $this->besar_model->by_id($id),
		));
	}
	public function hapus($id)
	{
		if ($this->agent->referrer() == '') show_404();
		db_delete('institusi_laporan', array('id' => $id));
		redirect($this->agent->referrer());
	}
	
}

/* End of file bengkel.php */
/* Location: ./application/modules/hpmp/controllers/bengkel.php */