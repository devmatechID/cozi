<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Laporan Bulanan Perusahaan Besar</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label col-sm-4"><span class="red">*</span> Perusahaan Besar</label>
				<div class="col-sm-6">
					<select class="form-control required" name="id_besar">
						<?php if (user_session('tingkatan') != 4): ?>
						<option value="">- Pilih Perusahaan Besar -</option>
						<?php endif; ?>
						<?php echo modules::run('options/besar', $data->id_besar); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Tahun Laporan</label>
				<div class="col-sm-7">
					<select class="form-control required" name="tahun">
						<?php echo modules::run('options/tahun', $data->tahun); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Bulan Laporan</label>
				<div class="col-sm-7">
					<select class="form-control required" name="bulan">
						<?php echo modules::run('options/bulan', $data->bulan); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-6">
			
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Pembelian HCFC</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="pembelian_hcfc" value="<?php echo $data->pembelian_hcfc; ?>">
				</div>
			</div>
			
		</div>
		<div class="col-md-6">
			
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Pembelian Preblended</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="pembelian_preblended" value="<?php echo $data->pembelian_preblended; ?>">
				</div>
			</div>
			
		</div>
		
		<div class="clearfix"></div>
		<div class="form-submit">
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
	</form>
</div>