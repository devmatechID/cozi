<link rel="stylesheet" href="<?php echo base_url()?>js/zebra_datepicker/public/css/default.css" type="text/css">  

<script type="text/javascript" src="<?php echo base_url()?>js/zebra_datepicker/public/javascript/zebra_datepicker.src.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.form.min.js"></script>
<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Laporan Institusi Pendidikan</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label col-sm-4"><span class="red">*</span> Institusi Pendidikan</label>
				<div class="col-sm-6">
					<select class="form-control required" name="id_institusi">
						<?php if (user_session('tingkatan') != 4): ?>
						<option value="">- Pilih Institusi Pendidikan -</option>
						<?php endif; ?>
						<?php echo modules::run('options/institusi', $data->id_institusi); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5">Tgl Pelatihan</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" id="tgl_pelatihan" name="tgl_pelatihan" value="<?php echo $data->tgl_pelatihan==null?date('Y-m-d'):$data->tgl_pelatihan; ?>">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5">Jenis Kegiatan</label>
				<div class="col-sm-7">
					<select class="form-control required" name="jenis_kegiatan">
					<option value="0" <?php if($data->jenis_kegiatan == ''){echo 'selected';}else if($data->jenis_kegiatan == '0'){echo 'selected';}else{echo '';}?> >PELATIHAN SAJA</option>
					<option value="1" <?php if($data->jenis_kegiatan == '1'){echo 'selected';}else{echo '';}?> >PELATIHAN & SERTIFIKASI</option>
					<!-- <input type="radio" name="jenis_kegiatan" value="0" <?php if($data->jenis_kegiatan == ''){echo 'checked';}else if($data->jenis_kegiatan == '0'){echo 'checked';}else{echo '';}?>/> Pelatihan saja
					<input type="radio" name="jenis_kegiatan" value="1" <?php if($data->jenis_kegiatan == '1'){echo 'checked';}else{echo '';}?>/> Pelatihan & Sertifikasi -->
					</select>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Peserta</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="peserta" value="<?php echo $data->peserta; ?>">
				</div>
			</div>
			
		</div>
		<div class="col-md-6">
			
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Peserta Lulus</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="peserta_lulus" value="<?php echo $data->peserta_lulus; ?>">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Sertifikasi</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="sertifikasi" value="<?php echo $data->sertifikasi; ?>">
				</div>
			</div>
		</div>
		
		<div class="clearfix"></div>
		<div class="form-submit">
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
	</form>
</div>
<script>
$('#tgl_pelatihan').Zebra_DatePicker({
		offset : [-300,100],
		format : 'Y-m-d'
	});
</script>