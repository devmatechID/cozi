<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Institusi_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'institusi_laporan_vw';
		
		$this->like = array();
		if (user_session('grup_pengguna') == 'supplier'){
		$this->filter = array (
			'id_institusi' => (user_session('grup_pengguna') == 'institusi') ? user_session('id_organisasi') : '',
		);
		}
		else if((user_session('grup_pengguna') == 'pemerintah') && ((user_session('tingkatan') == '3'))){
			$this->filter = array (
			'idkota' => user_session('id_kota'),
		);
		}
		else if((user_session('grup_pengguna') == 'pemerintah') && ((user_session('tingkatan') == '2'))){
			$this->filter = array (
			'id_propinsi' => user_session('id_propinsi'),
		);
		}
		else {
			
		}
		
		$this->fields = (object) array (
			'id_institusi' => '',
			'tgl_pelatihan' => '',
			'peserta' => '',
			'peserta_lulus' => '',
			'sertifikasi' => '',
			'jenis_kegiatan' => '',
		);
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*, b.nama AS institusi, c.nama AS pengguna,(case when $main_table.jenis_kegiatan = '0' then 'PELATIHAN SAJA' else 'PELATIHAN & SERTIFIKASI' end) jenis, tgl_pelatihan AS tanggal",FALSE);
		$this->db->join("institusi AS b", "$main_table.id_institusi = b.id", 'left');
		$this->db->join("pengguna AS c", "$main_table.created_by = c.id", 'left');
		$this->db->order_by(' created_at DESC, institusi');
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}
	
	
}
/* End of file bengkel_model.php */
/* Location: ./application/modules/hpmp/models/bengkel_model.php */