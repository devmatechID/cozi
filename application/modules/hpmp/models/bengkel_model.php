<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bengkel_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'bengkel_laporan_vw';
		
		$this->like = array();
		$this->filter = array (
			'id_bengkel' => (user_session('grup_pengguna') == 'bengkel') ? user_session('id_organisasi') : '',
			'id_propinsi' => (user_session('tingkatan') == '2') ? user_session('id_propinsi') : '',
			'idkota' => (user_session('tingkatan') == '3') ? user_session('id_kota'): '',
		);
		
		$this->fields = (object) array (
			'id_bengkel' => '',
			'tahun' => date('Y'),
			'bulan' => str_pad(date('m'), 2, '0', STR_PAD_LEFT),
			'ac_05_pk' => '',
			'ac_075_pk' => '',
			'ac_1_pk' => '',
			'ac_2_pk' => '',
			'ac_05_2' => '',
			'ac_3_10' => '',
			'ac_10' => '',
			'ac_cold' => '',
			'ac_mobil' => '',
			'ac_kulkas' => '',
			'ac_lainnya' => '',
			'r22' => '',
			'r134a' => '',
			'r32' => '',
			'r410a' => '',
			'r404a' => '',
			'r407a' => '',
			'r123' => '',
			'r290' => '',
			'amonia' => '',
			'co2' => '',
			'r600a' => '',
		);
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*, CONCAT(tahun, '-', bulan) AS tahun_bulan, b.nama AS bengkel, c.nama AS pengguna", FALSE);
		$this->db->join("bengkel AS b", "$main_table.id_bengkel = b.id", 'left');
		$this->db->join("pengguna AS c", "$main_table.created_by = c.id", 'left');
		$this->db->order_by($this->order.', created_at DESC, bengkel');
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}
	
	
}
/* End of file bengkel_model.php */
/* Location: ./application/modules/hpmp/models/bengkel_model.php */