<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Monitoring</h1>
</div>
<div id="main-container" class="container-fluid">
	<?php if (user_session('tingkatan')== 1 || user_session('tingkatan')==2 || user_session('tingkatan')==3 ):?>
	<form method="post" action="<?php echo $show_url; ?>">
		<div class="actions">
			<div class="btn-group pull-left">
				<a href="<?php echo $add_url; ?>" class="btn btn-success" title="Tambah Data"><i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs">Buat Monitoring</span></a>
			</div>
			<div class="clearfix"></div>
		</div>
	</form>
	<?php endif;?>
	<?php echo $grid; ?>
	<?php echo $pagelink; ?>
</div>