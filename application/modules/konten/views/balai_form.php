<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Balai Form</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label col-sm-3"><span class="red">*</span> Nama Balai</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="nama" value="<?php echo $data->nama; ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label col-sm-3">Propinsi</label>
				<div class="col-sm-7">
					<table>
						<tbody>
							<?php $i = 0; ?>
							<?php $i_last = 0; ?>
							<?php $list = explode(",", $data->list_id_propinsi); ?>
							
							<?php foreach( $propinsi->result() AS $p ) : ?>
								<?php $checked = ''; ?>
								<?php $color = ''; ?>
								
								<?php foreach($list AS $l) : ?>
									<?php if($l == $p->id){ $checked = 'checked'; $color = "style='color:red;'"; } ?>
								<?php endforeach; ?>
								
								<?php if( $i_last + 3 == $i ) { echo "<tr>"; $i_last = $i; } ?>
									<td style="padding-right:35px;">
										<label class="checkbox-inline" <?php echo $color; ?> ><input type="checkbox" name="id_propinsi[]" <?php echo $checked; ?> value="<?php echo $p->id; ?>"><?php echo $p->nama; ?></label>
									</td>
								<?php if( $i_last + 3 == $i ) { echo "</tr>"; $i_last = $i; } ?>
								<?php $i++; ?>
							<?php endforeach; ?>
							
						<tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div class="clearfix"></div>
		<div class="form-submit">
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
	</form>
</div>