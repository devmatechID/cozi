<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Balai_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'balai';
		
		$this->like = array();
		
		if (user_session('grup_pengguna') == 'besar'){
		$this->filter = array (
			'id_besar' => (user_session('grup_pengguna') == 'besar') ? user_session('id_organisasi') : '',
		);
		}
		else if((user_session('grup_pengguna') == 'pemerintah') && ((user_session('tingkatan') == '3'))){
			$this->filter = array (
			'idkota' => user_session('id_kota'),
		);
		}
		else if((user_session('grup_pengguna') == 'pemerintah') && ((user_session('tingkatan') == '2'))){
			$this->filter = array (
			'id_propinsi' => user_session('id_propinsi'),
		);
		}
		else {
			
		}
		
		$this->fields = (object) array (
			'id' => '',
			'nama' => '',
			'list_id_propinsi' => '',
		);
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		$this->db->limit($this->limit, $this->offset);

		return $this->db->get($main_table);
	}
	
	public function getPropinsi() {
		$this->db->where('deleted_at IS NULL');
		$this->db->order_by('id ASC');
		return $this->db->get('propinsi');
	}
	
	
}
/* End of file bengkel_model.php */
/* Location: ./application/modules/hpmp/models/bengkel_model.php */