<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengaduan_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'pengaduan';
		
		$this->like = array();
		$this->filter = array (
			'id_institusi' => (user_session('grup_pengguna') == 'institusi') ? user_session('id_organisasi') : '',
		);
		
		$this->fields = (object) array (
			'id_institusi' => '',
			'lokasi' => '',
			'id_kategori' => '',
			'judul' => '',
			'keterangan' => '',
			'nama_file' => '',
		);
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*, c.nama AS pengguna, k.nama AS kategori",FALSE);
		$this->db->join("pengguna AS c", "$main_table.created_by = c.id", 'left');
		$this->db->join("pengaduan_kategori AS k","$main_table.id_kategori = k.id","left");
		$this->db->order_by($this->order);
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}

	public function get_profil($id){

		$main_table = $this->table;

		$this->db->select("$main_table.*, m.nama_file, k.nama AS kategori",FALSE);
		$this->db->join("pengaduan_foto AS m","$main_table.id = m.id_pengaduan","left");
		$this->db->join("pengaduan_kategori AS k","$main_table.id_kategori = k.id","left");
		$this->db->where("$main_table.id", $id);
		$this->db->where("m.deleted_at IS NULL");
		$src = $this->db->get($main_table);
		return $src->num_rows() > 0 ? $src->row() : $this->fields;

	}

	public function get_picture($id){
		$query = "SELECT * FROM pengaduan_foto WHERE deleted_at IS NULL AND id_pengaduan = $id ORDER BY id DESC";

    	$src = $this->db->query($query);
    	return $src;
	}
	
	
}
/* End of file bengkel_model.php */
/* Location: ./application/modules/hpmp/models/bengkel_model.php */