<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Balai extends MX_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->page->use_directory();
		$this->load->model('balai_model');
	}
	
	
	public function index($q_encoded = 'x')
	{
		$this->balai_model->set_filter($q_encoded);
		
		$this->grid->init(array (
			'base_url' => $this->page->base_url("/index/$q_encoded"),
			'act_url' => $this->page->base_url(),
			'uri_segment' => 5,
			'items'	=> array (
				'nama' => array('text' => 'Nama'),
				//'list_id_propinsi' => array('text' => 'Propinsi', 'func' => 'getPropinsi'),
			),
			'num_rows' => $this->balai_model->num_rows(),
			'item' => 'nama',
			'order' => 'desc',
			'warning' => 'nama',
			'checkbox' => FALSE,
		));
		
		$this->balai_model->set_grid_params($this->grid->params());
		$this->grid->source($this->balai_model->get());
		
		$this->page->view('balai_index', array (
			'add_url' => $this->page->base_url('/tambah'),
			'insert_url' => $this->page->base_url('/insert'),
			'show_url' => $this->page->base_url('/show'),
			'filter' => (object) $this->balai_model->filter,
			'keyword' => $this->balai_model->keyword,
			'grid' => $this->grid->draw(),
			'pagelink' => $this->grid->page_link(),
		));
	}
	
	
	public function show()
	{
		$q_encoded = base64_encode(json_encode($this->input->post()));
		redirect($this->page->base_url("/index/$q_encoded"));
	}
	
	
	public function form($action = 'insert', $id = '')
	{
		if ($this->agent->referrer() == '') redirect($this->page->base_url());
		
		$this->page->view('balai_form', array (
			'action_url' => $this->page->base_url("/action/$id"),
			'redirect' => $this->agent->referrer(),
			'data' => $this->balai_model->by_id($id),
			'propinsi' => $this->balai_model->getPropinsi(),
		));
	}
	
	public function action($id = '') {
		$id_prop = "";
		foreach($this->input->post('id_propinsi') AS $ip) {
			$id_prop .= $ip.",";
		}
		
		$data = array(
			'nama' => strtoupper( $this->input->post('nama') ),
			'list_id_propinsi' => substr($id_prop,0,-1),
		);
		
		if($id == '') {
			db_insert('balai', $data);
		} else {
			db_update('balai', $data, array('id' => $id));
		}
		
		redirect($this->input->post('redirect'));
	}
	
	
	public function tambah()
	{
		$this->form();
	}
	
	
	public function ubah($id)
	{
		$this->form('update', $id);
	}
	
	public function hapus($id) {
		db_delete('balai', "id = $id");
		redirect( $this->agent->referrer() );
	}
	
}

/* End of file bengkel.php */
/* Location: ./application/modules/hpmp/controllers/bengkel.php */