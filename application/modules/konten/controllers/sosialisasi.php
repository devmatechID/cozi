<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sosialisasi extends MX_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->page->use_directory();
		$this->load->model('sosialisasi_model');
	}
	
	
	public function index($q_encoded = 'x')
	{
		$this->sosialisasi_model->set_filter($q_encoded);
		
		$this->grid->init(array (
			'base_url' => $this->page->base_url("/index/$q_encoded"),
			'act_url' => $this->page->base_url(),
			'uri_segment' => 5,
			'items'	=> array (
				'created_at' => array('text' => 'Waktu Input'),
				'kegiatan' => array('text' => 'Kegiatan', 'link' => $this->page->base_url('/profil')),
				'tgl_sosialisasi' => array('text' => 'Tanggal Sosialisasi'),
				'tempat' => array('text' => 'Tempat'),
				'pengguna' => array('text' => 'Pengguna Input'),
			),
			'num_rows' => $this->sosialisasi_model->num_rows(),
			'item' => 'created_at',
			'order' => 'desc',
			'warning' => 'created_by',
			'checkbox' => FALSE,
		));

		if(user_session('grup_pengguna')!='superadmin'){
			$this->grid->init(array(
			 'user_id' => user_session('id'),
			));
		}
		
		$this->sosialisasi_model->set_grid_params($this->grid->params());
		$this->grid->source($this->sosialisasi_model->get());
		if(user_session('tingkatan')!=1 && user_session('tingkatan')!=2 && user_session('tingkatan')!=3){	
		$this->grid->disable_all_acts();
		}

		$this->page->view('sosialisasi_index', array (
			'add_url' => $this->page->base_url('/tambah'),
			'insert_url' => $this->page->base_url('/insert'),
			'show_url' => $this->page->base_url('/show'),
			'filter' => (object) $this->sosialisasi_model->filter,
			'keyword' => $this->sosialisasi_model->keyword,
			'grid' => $this->grid->draw(),
			'pagelink' => $this->grid->page_link(),
		));
	}
	
	
	public function show()
	{
		$q_encoded = base64_encode(json_encode($this->input->post()));
		redirect($this->page->base_url("/index/$q_encoded"));
	}
	
	
	public function form($action = 'insert', $id = '')
	{
		if ($this->agent->referrer() == '') redirect($this->page->base_url());
		
		$this->page->view('sosialisasi_form', array (
			'action_url' => $this->page->base_url("/$action/$id"),
			'redirect' => $this->agent->referrer(),
			'data' => $this->sosialisasi_model->by_id($id),
		));
	}
	
	
	public function tambah()
	{
		$this->form();
	}
	
	
	public function ubah($id)
	{
		$this->form('update', $id);
	}
	
	
	public function form_data()
	{
		$names = array('upper_kegiatan', 'tgl_sosialisasi','upper_tempat','artikel','id_kota');
		return form_data($names);
	}

	public function insert()
	{
		$filename = $this->input->post("filename");
		$data = $this->form_data();
		$video = $this->input->post('videosrc');
		$media = $this->input->post('media');
		// print_r($data);die();
		// $data['tgl_pelatihan'] = $tgl;


	    db_insert('sosialisasi', $data);
		$id_sol = $this->db->insert_id();


		if(! empty($video)){
			foreach ($video as $key => $value) {
				db_insert('sosialisasi_foto',array("id_sosialisasi" => $id_sol, "url"=> $value, "jenis_media"=> '1') );
			}
		}

		// if($media == 0)
		if(isset($_FILES['filename']) && count($_FILES['filename']['error']) == 1 && $_FILES['filename']['error'][0] > 0){
		    
		} else if(isset($_FILES['filename'])){ 

			$this->upload($id_sol);
		}	

		redirect($this->input->post('redirect'));
	}
	
	function upload($id){

		$upload_path = './img/sosialisasi/'.$id;

		if ( ! file_exists($upload_path)) mkdir($upload_path, 0777); 
		
		$data = array();
        if(!empty($_FILES['filename']['name'])){
            $filesCount = count($_FILES['filename']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['userFile']['name'] = $_FILES['filename']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['filename']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['filename']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['filename']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['filename']['size'][$i];

                $uploadPath = 'uploads/files/';
                $config['upload_path'] = $upload_path;
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['encrypt_name'] = TRUE;
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                  
                  	$data_foto = array('id_sosialisasi'=> $id, 'nama_file' => $fileData['file_name'], 'jenis_media' => '0');

                    db_insert("sosialisasi_foto",$data_foto);
                }
            }
            
        }		

	}

	public function update($id)
	{
		$video = $this->input->post('videosrc');
		$media = $this->input->post('media');

		$data = $this->form_data();
		db_update('sosialisasi', $data, array('id' => $id));

		// if($media == 1)
		if(! empty($video)){
		db_delete('sosialisasi_foto',array("id_sosialisasi" => $id,'jenis_media'=> '1'));
		foreach ($video as $key => $value) {
			db_insert('sosialisasi_foto',array("id_sosialisasi" => $id, "url"=> $value, "jenis_media"=> '1') );
		}
		}
		
		// if($media == 0)
		if(isset($_FILES['filename']) && count($_FILES['filename']['error']) == 1 && $_FILES['filename']['error'][0] > 0){
		    
		} else if(isset($_FILES['filename'])){ 
			db_delete('sosialisasi_foto',array("id_sosialisasi" => $id,"jenis_media"=> '0'));
			$this->upload($id);
		}
		

		redirect($this->input->post('redirect'));
	}
	
	
	public function profil($id)
	{
		$this->page->view('sosialisasi_profil', array (
			'data' => $this->sosialisasi_model->get_profil($id),
			'pict' => $this->sosialisasi_model->get_picture($id),
		));
	}

	public function hapus($id)
	{
		if ($this->agent->referrer() == '') show_404();
		db_delete('sosialisasi', array('id' => $id));
		redirect($this->agent->referrer());
	}
}