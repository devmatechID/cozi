<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tips extends MX_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->page->use_directory();
		$this->load->model('tips_model');
	}
	
	
	public function index($q_encoded = 'x')
	{
		$this->tips_model->set_filter($q_encoded);
		
		$this->grid->init(array (
			'base_url' => $this->page->base_url("/index/$q_encoded"),
			'act_url' => $this->page->base_url(),
			'uri_segment' => 5,
			'items'	=> array (
				'created_at' => array('text' => 'Waktu Input'),
				'judul' => array('text' => 'Judul', 'link' => $this->page->base_url('/profil')),
				'status' => array('text' => 'Status', 'func' => 'aktif_pasif'),
				'pengguna' => array('text' => 'Pengguna Input'),
			),
			'num_rows' => $this->tips_model->num_rows(),
			'item' => 'created_at',
			'order' => 'desc',
			'warning' => 'judul',
			'checkbox' => FALSE,
		));
		
		$this->tips_model->set_grid_params($this->grid->params());
		$this->grid->source($this->tips_model->get());

		if(user_session('tingkatan')!=1){	
		$this->grid->disable_all_acts();
		}
		
		$this->page->view('tips_index', array (
			'add_url' => $this->page->base_url('/tambah'),
			'insert_url' => $this->page->base_url('/insert'),
			'show_url' => $this->page->base_url('/show'),
			'filter' => (object) $this->tips_model->filter,
			'keyword' => $this->tips_model->keyword,
			'grid' => $this->grid->draw(),
			'pagelink' => $this->grid->page_link(),
		));
	}
	
	
	public function show()
	{
		$q_encoded = base64_encode(json_encode($this->input->post()));
		redirect($this->page->base_url("/index/$q_encoded"));
	}
	
	
	public function form($action = 'insert', $id = '')
	{
		if ($this->agent->referrer() == '') redirect($this->page->base_url());
		
		$this->page->view('tips_form', array (
			'action_url' => $this->page->base_url("/$action/$id"),
			'redirect' => $this->agent->referrer(),
			'data' => $this->tips_model->by_id($id),
		));
	}
	
	
	public function tambah()
	{
		$this->form();
	}
	
	
	public function ubah($id)
	{
		$this->form('update', $id);
	}
	
	
	public function form_data()
	{
		$names = array('upper_judul', 'status','artikel', 'jenis_media');
		return form_data($names);
	}
	
	
	public function insert()
	{
		$data = $this->form_data();

		if($data['jenis_media']==1)
		$data['url'] = $this->input->post('url');

		db_insert('tips', $data);

		if($data['jenis_media']==0)
		$this->upload($this->db->insert_id());
		redirect($this->input->post('redirect'));
	}
	
	
	public function update($id)
	{
		$data = $this->form_data();

		if($data['jenis_media']==1)
		$data['url'] = $this->input->post('url');

		db_update('tips', $data, array('id' => $id));

		if($data['jenis_media']==0)
		$this->upload($id);

		redirect($this->input->post('redirect'));
	}

	public function upload($id_tips){

		$upload_path = './img/tips/'.$id_tips;
		
		if ( ! file_exists($upload_path)) mkdir($upload_path, 0777); 
		
		$this->load->library('upload', array (
			'upload_path' => $upload_path,
			'allowed_types' => "gif|jpg|png|jpeg",
			'encrypt_name' => TRUE,
			'max_size' => 2048,
			'max_width' => 3600,
			'max_height' => 2410,
		));
		// print_r($_FILES['foto']['name']);
		if($_FILES['foto']['name']!=''){
		if ($this->upload->do_upload('foto')) {
			
			$metadata = $this->upload->data();
			$nama_file = $metadata['file_name'];
			
			$foto = array (
				'nama_file' => $nama_file,
			);
			
			db_update('tips', $foto, array('id' => $id_tips));
		}
		else {
			// $this->session->set_flashdata('upload_err', );
			print_r($this->upload->display_errors('', ''));
		}
		}
		// print_r($this->upload->data());
	}
	
	
	public function profil($id)
	{
		$this->page->view('tips_profil', array (
			'data' => $this->tips_model->by_id($id),
		));
	}

	public function hapus($id)
	{
		if ($this->agent->referrer() == '') show_404();
		db_delete('tips', array('id' => $id));
		redirect($this->agent->referrer());
	}
	
}

/* End of file bengkel.php */
/* Location: ./application/modules/hpmp/controllers/bengkel.php */