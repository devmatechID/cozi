<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Rekap Penggunaan HCFC Kota</h1>
</div>
<div id="main-container" class="container-fluid">
	<div class="clearfix"></div>
	<br>
	<form method="post" action="<?php echo $show_url; ?>">
		<div class="actions">
			Produk
			<select class="col-sm-2 col-md-2" name="kriteria">
				<?php 
						$hcfc='';
						$preblended='';
						if($krit=='hcfc'){
							$hcfc='selected';
						}elseif ($krit=='preblended') {
							$preblended='selected';
						}
					?>
					<option value="hcfc" <?php echo $hcfc;?> >HCFC</option>
					<option value="preblended" <?php echo $preblended;?>>Preblended</option>
			</select>
			Tahun
			<select class="col-sm-2 col-md-2" name="tahun">
				<?php echo modules::run('options/tahun', $tahun); ?>
			</select>
			<?php if (user_session('tingkatan') == '1' || user_session('tingkatan') == '2'): ?>
			Propinsi
			<select class="col-sm-2 col-md-2" name="id_kota">
				<?php echo modules::run('options/kotaku', $id_kota); ?>
			</select>
			<?php endif; ?>	
			<button class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i></button>
			<div class="clearfix"></div>
		</div>
	</form>
	<div id="num-data"><i class="glyphicon glyphicon-stats"></i>&nbsp; <?php echo $total_data; ?> data</div>
	<div class="col-sm-12">
	<div class="table-responsive">
		<table class="table table-profile table-striped table-condensed">
			<thead>
				<tr>
					<th>No.</th>
					<th>Perusahaan</th>
					<th>JAN</th>
					<th>FEB</th>
					<th>MAR</th>
					<th>APR</th>
					<th>MEI</th>
					<th>JUN</th>
					<th>JUL</th>
					<th>AGS</th>
					<th>SEP</th>
					<th>OKT</th>
					<th>NOV</th>
					<th>DES</th>
					<th><?php echo $tahun; ?></th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$no=$offset;
					foreach ($propinsi->result() as $p): 
					$no++;
					$total=($p->JAN+$p->FEB+$p->MAR+$p->APR+$p->MEI+$p->JUN+$p->JUL+$p->AGS+$p->SEP+$p->OKT+$p->NOV+$p->DES);
				?>
				<tr>
					<td><?php echo $no; ?></td>
					<td>
					<a href="<?php echo $this->page->base_url('/profil_kota/'.$p->id.'/'.$tahun.'/'.$krit); ?>">
					<?php echo $p->nama; ?>
					</td>
					<td align="center"><?php echo $p->JAN; ?></td>
					<td align="center"><?php echo $p->FEB; ?></td>
					<td align="center"><?php echo $p->MAR; ?></td>
					<td align="center"><?php echo $p->APR; ?></td>
					<td align="center"><?php echo $p->MEI; ?></td>
					<td align="center"><?php echo $p->JUN; ?></td>
					<td align="center"><?php echo $p->JUL; ?></td>
					<td align="center"><?php echo $p->AGS; ?></td>
					<td align="center"><?php echo $p->SEP; ?></td>
					<td align="center"><?php echo $p->OKT; ?></td>
					<td align="center"><?php echo $p->NOV; ?></td>
					<td align="center"><?php echo $p->DES; ?></td>
					<td align="center"><?php echo round($total,2); ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	</div>
	<?php echo $page_link; ?> 
</div>
<style type="text/css" media="screen">
	.divkiri{
		float:left;
		width: 80%;
	}
	.divkanan{
		float: right;
		width: 15%;
	}
	.outer {
	    display: table;
	    position: absolute;
	    height: 100%;
	    width: 100%;
	}

	.middle {
	    display: table-cell;
	    vertical-align: middle;
	}

	.inner {
	    margin-left: auto;
	    margin-right: auto; 
	    /*width: /*whatever width you want*/;*/
	}
</style>
	