<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Freon_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
	
	}
	
	
	public function get($tahun,$bulan,$krit)
	{
		$filter="";
		if($krit==1)
			$filter=" AND bulan='$bulan'";
		$sql="SELECT p.id, p.nama, 
				CASE WHEN `r22` IS NULL THEN 0.00 ELSE ROUND(`r22`,2) END AS r22,
				CASE WHEN `r134a` IS NULL THEN 0.00 ELSE ROUND(`r134a`,2) END AS r134a,
				CASE WHEN `r32` IS NULL THEN 0.00 ELSE ROUND(`r32`,2) END AS r32,
				CASE WHEN `r410a` IS NULL THEN 0.00 ELSE ROUND(`r410a`,2) END AS r410a,
				CASE WHEN `r404a` IS NULL THEN 0.00 ELSE ROUND(`r404a`,2) END AS r404a,
				CASE WHEN `r407a` IS NULL THEN 0.00 ELSE ROUND(`r407a`,2) END AS r407a,
				CASE WHEN `r123` IS NULL THEN 0.00 ELSE ROUND(`r123`,2) END AS r123,
				CASE WHEN `r290` IS NULL THEN 0.00 ELSE ROUND(`r290`,2) END AS r290,
				CASE WHEN `r600a` IS NULL THEN 0.00 ELSE ROUND(`r600a`,2) END AS r600a,
				CASE WHEN `amonia` IS NULL THEN 0.00 ELSE ROUND(`amonia`,2) END AS amonia,
				CASE WHEN `co2` IS NULL THEN 0.00 ELSE ROUND(`co2`,2) END AS co2
			FROM propinsi p
			LEFT JOIN
			(SELECT b.id_propinsi, 
			SUM(`r22`) AS r22,
			SUM(`r134a`) AS r134a,
			SUM(`r32`) AS r32,
			SUM(`r410a`) AS r410a,
			SUM(`r404a`) AS r404a,
			SUM(`r407a`) AS r407a,
			SUM(`r123`) AS r123,
			SUM(`r290`) AS r290,
			SUM(`r600a`) AS r600a,
			SUM(`amonia`) AS amonia,
			SUM(`co2`) AS co2
			FROM bengkel_vw b
			LEFT JOIN 
			(SELECT id, id_bengkel,
				CASE WHEN `r22` IS NULL THEN 0 ELSE `r22` END AS r22,
				CASE WHEN `r134a` IS NULL THEN 0 ELSE `r134a` END AS r134a,
				CASE WHEN `r32` IS NULL THEN 0 ELSE `r32` END AS r32,
				CASE WHEN `r410a` IS NULL THEN 0 ELSE `r410a` END AS r410a,
				CASE WHEN `r404a` IS NULL THEN 0 ELSE `r404a` END AS r404a,
				CASE WHEN `r407a` IS NULL THEN 0 ELSE `r407a` END AS r407a,
				CASE WHEN `r123` IS NULL THEN 0 ELSE `r123` END AS r123,
				CASE WHEN `r290` IS NULL THEN 0 ELSE `r290` END AS r290,
				CASE WHEN `r600a` IS NULL THEN 0 ELSE `r600a` END AS r600a,
				CASE WHEN `amonia` IS NULL THEN 0 ELSE `amonia` END AS amonia,
				CASE WHEN `co2` IS NULL THEN 0 ELSE `co2` END AS co2
			FROM bengkel_laporan l
			WHERE deleted_at IS NULL AND tahun={$tahun} $filter)bl ON bl.id_bengkel=b.id
			WHERE b.deleted_at IS NULL
			GROUP BY b.id_propinsi)bw ON bw.id_propinsi=p.id
			WHERE p.deleted_at IS NULL";
		return $this->db->query($sql);
	}
		
	public function get_kota($id_propinsi, $tahun, $bulan, $krit, $off = -1, $limit = 20)
	{
		$filter="";
		if($krit==1)
			$filter=" AND bulan='$bulan'";
		$pagging = '';
		if($off >= 0)
		{
			$pagging = " LIMIT $off, $limit";
		}

		$sql="SELECT p.id, p.nama, 
				CASE WHEN `r22` IS NULL THEN 0.00 ELSE ROUND(`r22`,2) END AS r22,
				CASE WHEN `r134a` IS NULL THEN 0.00 ELSE ROUND(`r134a`,2) END AS r134a,
				CASE WHEN `r32` IS NULL THEN 0.00 ELSE ROUND(`r32`,2) END AS r32,
				CASE WHEN `r410a` IS NULL THEN 0.00 ELSE ROUND(`r410a`,2) END AS r410a,
				CASE WHEN `r404a` IS NULL THEN 0.00 ELSE ROUND(`r404a`,2) END AS r404a,
				CASE WHEN `r407a` IS NULL THEN 0.00 ELSE ROUND(`r407a`,2) END AS r407a,
				CASE WHEN `r123` IS NULL THEN 0.00 ELSE ROUND(`r123`,2) END AS r123,
				CASE WHEN `r290` IS NULL THEN 0.00 ELSE ROUND(`r290`,2) END AS r290,
				CASE WHEN `r600a` IS NULL THEN 0.00 ELSE ROUND(`r600a`,2) END AS r600a,
				CASE WHEN `amonia` IS NULL THEN 0.00 ELSE ROUND(`amonia`,2) END AS amonia,
				CASE WHEN `co2` IS NULL THEN 0.00 ELSE ROUND(`co2`,2) END AS co2
			FROM kota p
			LEFT JOIN
			(SELECT b.id_kota, 
			SUM(`r22`) AS r22,
			SUM(`r134a`) AS r134a,
			SUM(`r32`) AS r32,
			SUM(`r410a`) AS r410a,
			SUM(`r404a`) AS r404a,
			SUM(`r407a`) AS r407a,
			SUM(`r123`) AS r123,
			SUM(`r290`) AS r290,
			SUM(`r600a`) AS r600a,
			SUM(`amonia`) AS amonia,
			SUM(`co2`) AS co2
			FROM bengkel_vw b
			LEFT JOIN 
			(SELECT id, id_bengkel,
				CASE WHEN `r22` IS NULL THEN 0 ELSE `r22` END AS r22,
				CASE WHEN `r134a` IS NULL THEN 0 ELSE `r134a` END AS r134a,
				CASE WHEN `r32` IS NULL THEN 0 ELSE `r32` END AS r32,
				CASE WHEN `r410a` IS NULL THEN 0 ELSE `r410a` END AS r410a,
				CASE WHEN `r404a` IS NULL THEN 0 ELSE `r404a` END AS r404a,
				CASE WHEN `r407a` IS NULL THEN 0 ELSE `r407a` END AS r407a,
				CASE WHEN `r123` IS NULL THEN 0 ELSE `r123` END AS r123,
				CASE WHEN `r290` IS NULL THEN 0 ELSE `r290` END AS r290,
				CASE WHEN `r600a` IS NULL THEN 0 ELSE `r600a` END AS r600a,
				CASE WHEN `amonia` IS NULL THEN 0 ELSE `amonia` END AS amonia,
				CASE WHEN `co2` IS NULL THEN 0 ELSE `co2` END AS co2
			FROM bengkel_laporan l
			WHERE deleted_at IS NULL AND tahun={$tahun} $filter)bl ON bl.id_bengkel=b.id
			WHERE b.deleted_at IS NULL
			GROUP BY b.id_kota)bw ON bw.id_kota=p.id
			WHERE p.deleted_at IS NULL AND p.id_propinsi=$id_propinsi
			$pagging";
			
		return $this->db->query($sql);
	}

	public function get_kota_detil($id_kota, $tahun, $bulan, $krit, $off = -1, $limit = 20)
	{
		$filter="";
		if($krit==1)
			$filter=" AND bulan='$bulan'";
		$pagging = '';
		if($off >= 0)
		{
			$pagging = " LIMIT $off, $limit";
		}

		$sql="SELECT b.id, b.nama,
			CASE WHEN SUM(`r22`) IS NULL THEN 0.00 ELSE ROUND(SUM(`r22`),2) END AS r22,
			CASE WHEN SUM(`r134a`) IS NULL THEN 0.00 ELSE ROUND(SUM(`r134a`),2) END AS r134a,
			CASE WHEN SUM(`r32`) IS NULL THEN 0.00 ELSE ROUND(SUM(`r32`),2) END AS r32,
			CASE WHEN SUM(`r410a`) IS NULL THEN 0.00 ELSE ROUND(SUM(`r410a`),2) END AS r410a,
			CASE WHEN SUM(`r404a`) IS NULL THEN 0.00 ELSE ROUND(SUM(`r404a`),2) END AS r404a,
			CASE WHEN SUM(`r407a`) IS NULL THEN 0.00 ELSE ROUND(SUM(`r407a`),2) END AS r407a,
			CASE WHEN SUM(`r123`) IS NULL THEN 0.00 ELSE ROUND(SUM(`r123`),2) END AS r123,
			CASE WHEN SUM(`r290`) IS NULL THEN 0.00 ELSE ROUND(SUM(`r290`),2) END AS r290,
			CASE WHEN SUM(`r600a`) IS NULL THEN 0.00 ELSE ROUND(SUM(`r600a`),2) END AS r600a,
			CASE WHEN SUM(`amonia`) IS NULL THEN 0.00 ELSE ROUND(SUM(`amonia`),2) END AS amonia,
			CASE WHEN SUM(`co2`) IS NULL THEN 0.00 ELSE ROUND(SUM(`co2`),2) END AS co2
			FROM bengkel_vw b
			LEFT JOIN 
			(SELECT id, id_bengkel,
				CASE WHEN `r22` IS NULL THEN 0 ELSE `r22` END AS r22,
				CASE WHEN `r134a` IS NULL THEN 0 ELSE `r134a` END AS r134a,
				CASE WHEN `r32` IS NULL THEN 0 ELSE `r32` END AS r32,
				CASE WHEN `r410a` IS NULL THEN 0 ELSE `r410a` END AS r410a,
				CASE WHEN `r404a` IS NULL THEN 0 ELSE `r404a` END AS r404a,
				CASE WHEN `r407a` IS NULL THEN 0 ELSE `r407a` END AS r407a,
				CASE WHEN `r123` IS NULL THEN 0 ELSE `r123` END AS r123,
				CASE WHEN `r290` IS NULL THEN 0 ELSE `r290` END AS r290,
				CASE WHEN `r600a` IS NULL THEN 0 ELSE `r600a` END AS r600a,
				CASE WHEN `amonia` IS NULL THEN 0 ELSE `amonia` END AS amonia,
				CASE WHEN `co2` IS NULL THEN 0 ELSE `co2` END AS co2
			FROM bengkel_laporan l
			WHERE deleted_at IS NULL AND tahun={$tahun} $filter)bl ON bl.id_bengkel=b.id
			WHERE b.deleted_at IS NULL AND b.id_kota=$id_kota
			$pagging ";
			
		return $this->db->query($sql);
	}
}
/* End of file freon_model.php */
/* Location: ./application/modules/rekap/models/freon_model.php */