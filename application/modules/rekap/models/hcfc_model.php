<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hcfc_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
	
	}
	
	
	public function get($tahun, $krit)
	{
		if($krit=='hcfc'){
			$besar="pembelian_hcfc";
			$supplier="produk ='hcfc'";
			$ukm="0";
		}else{
			$besar="pembelian_preblended";
			$supplier="produk ='preblended'";
			$ukm="pembelian_preblended";
		}
		$sql="SELECT p.id, p.nama,
					CASE WHEN JAN IS NULL THEN 0.00 ELSE ROUND(JAN,2) END AS JAN,
					CASE WHEN FEB IS NULL THEN 0.00 ELSE ROUND(FEB,2) END AS FEB,
					CASE WHEN MAR IS NULL THEN 0.00 ELSE ROUND(MAR,2) END AS MAR,
					CASE WHEN APR IS NULL THEN 0.00 ELSE ROUND(APR,2) END AS APR,
					CASE WHEN MEI IS NULL THEN 0.00 ELSE ROUND(MEI,2) END AS MEI,
					CASE WHEN JUN IS NULL THEN 0.00 ELSE ROUND(JUN,2) END AS JUN,
					CASE WHEN JUL IS NULL THEN 0.00 ELSE ROUND(JUL,2) END AS JUL,
					CASE WHEN AGS IS NULL THEN 0.00 ELSE ROUND(AGS,2) END AS AGS,
					CASE WHEN SEP IS NULL THEN 0.00 ELSE ROUND(SEP,2) END AS SEP,
					CASE WHEN OKT IS NULL THEN 0.00 ELSE ROUND(OKT,2) END AS OKT,
					CASE WHEN NOV IS NULL THEN 0.00 ELSE ROUND(NOV,2) END AS NOV,
					CASE WHEN DES IS NULL THEN 0.00 ELSE ROUND(DES,2) END AS DES
					FROM propinsi p 
					LEFT JOIN
					(SELECT pv.id_propinsi,
						SUM(CASE WHEN JAN IS NULL THEN 0 ELSE JAN END) AS JAN,
						SUM(CASE WHEN FEB IS NULL THEN 0 ELSE FEB END) AS FEB,
						SUM(CASE WHEN MAR IS NULL THEN 0 ELSE MAR END) AS MAR,
						SUM(CASE WHEN APR IS NULL THEN 0 ELSE APR END) AS APR,
						SUM(CASE WHEN MEI IS NULL THEN 0 ELSE MEI END) AS MEI,
						SUM(CASE WHEN JUN IS NULL THEN 0 ELSE JUN END) AS JUN,
						SUM(CASE WHEN JUL IS NULL THEN 0 ELSE JUL END) AS JUL,
						SUM(CASE WHEN AGS IS NULL THEN 0 ELSE AGS END) AS AGS,
						SUM(CASE WHEN SEP IS NULL THEN 0 ELSE SEP END) AS SEP,
						SUM(CASE WHEN OKT IS NULL THEN 0 ELSE OKT END) AS OKT,
						SUM(CASE WHEN NOV IS NULL THEN 0 ELSE NOV END) AS NOV,
						SUM(CASE WHEN DES IS NULL THEN 0 ELSE DES END) AS DES
						FROM
						(SELECT id_besar AS id_organisasi, tahun, 'besar' AS perusahaan,
							CASE WHEN SUM(CASE WHEN bulan ='01' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS JAN,
							CASE WHEN SUM(CASE WHEN bulan ='02' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS FEB,
							CASE WHEN SUM(CASE WHEN bulan ='03' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS MAR,
							CASE WHEN SUM(CASE WHEN bulan ='04' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS APR,
							CASE WHEN SUM(CASE WHEN bulan ='05' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS MEI,
							CASE WHEN SUM(CASE WHEN bulan ='06' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS JUN,
							CASE WHEN SUM(CASE WHEN bulan ='07' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS JUL,
							CASE WHEN SUM(CASE WHEN bulan ='08' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS AGS,
							CASE WHEN SUM(CASE WHEN bulan ='09' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS SEP,
							CASE WHEN SUM(CASE WHEN bulan ='10' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS OKT,
							CASE WHEN SUM(CASE WHEN bulan ='11' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS NOV,
							CASE WHEN SUM(CASE WHEN bulan ='12' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS DES
							FROM besar_laporan
							WHERE deleted_at IS NULL
							GROUP BY id_besar, tahun
						UNION ALL
						SELECT id_supplier AS id_organisasi, tahun, 'supplier' AS perusahaan,
							CASE WHEN SUM(CASE WHEN bulan ='01' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS JAN,
							CASE WHEN SUM(CASE WHEN bulan ='02' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS FEB,
							CASE WHEN SUM(CASE WHEN bulan ='03' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS MAR,
							CASE WHEN SUM(CASE WHEN bulan ='04' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS APR,
							CASE WHEN SUM(CASE WHEN bulan ='05' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS MEI,
							CASE WHEN SUM(CASE WHEN bulan ='06' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS JUN,
							CASE WHEN SUM(CASE WHEN bulan ='07' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS JUL,
							CASE WHEN SUM(CASE WHEN bulan ='08' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS AGS,
							CASE WHEN SUM(CASE WHEN bulan ='09' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS SEP,
							CASE WHEN SUM(CASE WHEN bulan ='10' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS OKT,
							CASE WHEN SUM(CASE WHEN bulan ='11' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS NOV,
							CASE WHEN SUM(CASE WHEN bulan ='12' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS DES
							FROM supplier_laporan
							WHERE deleted_at IS NULL
							GROUP BY id_supplier, tahun
						UNION ALL
						SELECT id_ukm AS id_organisasi, tahun, 'ukm' AS perusahaan,
							CASE WHEN SUM(CASE WHEN bulan ='01' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS JAN,
							CASE WHEN SUM(CASE WHEN bulan ='02' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS FEB,
							CASE WHEN SUM(CASE WHEN bulan ='03' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS MAR,
							CASE WHEN SUM(CASE WHEN bulan ='04' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS APR,
							CASE WHEN SUM(CASE WHEN bulan ='05' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS MEI,
							CASE WHEN SUM(CASE WHEN bulan ='06' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS JUN,
							CASE WHEN SUM(CASE WHEN bulan ='07' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS JUL,
							CASE WHEN SUM(CASE WHEN bulan ='08' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS AGS,
							CASE WHEN SUM(CASE WHEN bulan ='09' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS SEP,
							CASE WHEN SUM(CASE WHEN bulan ='10' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS OKT,
							CASE WHEN SUM(CASE WHEN bulan ='11' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS NOV,
							CASE WHEN SUM(CASE WHEN bulan ='12' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS DES
							FROM ukm_laporan
							WHERE deleted_at IS NULL
							GROUP BY id_ukm, tahun)la
					JOIN perusahaan_vw pv ON pv.id=la.id_organisasi AND pv.perusahaan=la.perusahaan
					WHERE la.tahun={$tahun}
					GROUP BY id_propinsi)pvv ON pvv.id_propinsi=p.id";
		return $this->db->query($sql);
	}
		
	public function get_kota($id_propinsi, $tahun, $krit, $off = -1, $limit = 20)
	{
		if($krit=='hcfc'){
			$besar="pembelian_hcfc";
			$supplier="produk ='hcfc'";
			$ukm="0";
		}else{
			$besar="pembelian_preblended";
			$supplier="produk ='preblended'";
			$ukm="pembelian_preblended";
		}
		
		$pagging = '';
		if($off >= 0)
		{
			$pagging = " LIMIT $off, $limit";
		}

		$sql="SELECT p.id, p.nama,
					CASE WHEN JAN IS NULL THEN 0.00 ELSE ROUND(JAN,2) END AS JAN,
					CASE WHEN FEB IS NULL THEN 0.00 ELSE ROUND(FEB,2) END AS FEB,
					CASE WHEN MAR IS NULL THEN 0.00 ELSE ROUND(MAR,2) END AS MAR,
					CASE WHEN APR IS NULL THEN 0.00 ELSE ROUND(APR,2) END AS APR,
					CASE WHEN MEI IS NULL THEN 0.00 ELSE ROUND(MEI,2) END AS MEI,
					CASE WHEN JUN IS NULL THEN 0.00 ELSE ROUND(JUN,2) END AS JUN,
					CASE WHEN JUL IS NULL THEN 0.00 ELSE ROUND(JUL,2) END AS JUL,
					CASE WHEN AGS IS NULL THEN 0.00 ELSE ROUND(AGS,2) END AS AGS,
					CASE WHEN SEP IS NULL THEN 0.00 ELSE ROUND(SEP,2) END AS SEP,
					CASE WHEN OKT IS NULL THEN 0.00 ELSE ROUND(OKT,2) END AS OKT,
					CASE WHEN NOV IS NULL THEN 0.00 ELSE ROUND(NOV,2) END AS NOV,
					CASE WHEN DES IS NULL THEN 0.00 ELSE ROUND(DES,2) END AS DES
					FROM kota p 
					LEFT JOIN
					(SELECT pv.id_kota,
						SUM(CASE WHEN JAN IS NULL THEN 0 ELSE JAN END) AS JAN,
						SUM(CASE WHEN FEB IS NULL THEN 0 ELSE FEB END) AS FEB,
						SUM(CASE WHEN MAR IS NULL THEN 0 ELSE MAR END) AS MAR,
						SUM(CASE WHEN APR IS NULL THEN 0 ELSE APR END) AS APR,
						SUM(CASE WHEN MEI IS NULL THEN 0 ELSE MEI END) AS MEI,
						SUM(CASE WHEN JUN IS NULL THEN 0 ELSE JUN END) AS JUN,
						SUM(CASE WHEN JUL IS NULL THEN 0 ELSE JUL END) AS JUL,
						SUM(CASE WHEN AGS IS NULL THEN 0 ELSE AGS END) AS AGS,
						SUM(CASE WHEN SEP IS NULL THEN 0 ELSE SEP END) AS SEP,
						SUM(CASE WHEN OKT IS NULL THEN 0 ELSE OKT END) AS OKT,
						SUM(CASE WHEN NOV IS NULL THEN 0 ELSE NOV END) AS NOV,
						SUM(CASE WHEN DES IS NULL THEN 0 ELSE DES END) AS DES
						FROM
						(SELECT id_besar AS id_organisasi, tahun, 'besar' AS perusahaan,
							CASE WHEN SUM(CASE WHEN bulan ='01' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS JAN,
							CASE WHEN SUM(CASE WHEN bulan ='02' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS FEB,
							CASE WHEN SUM(CASE WHEN bulan ='03' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS MAR,
							CASE WHEN SUM(CASE WHEN bulan ='04' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS APR,
							CASE WHEN SUM(CASE WHEN bulan ='05' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS MEI,
							CASE WHEN SUM(CASE WHEN bulan ='06' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS JUN,
							CASE WHEN SUM(CASE WHEN bulan ='07' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS JUL,
							CASE WHEN SUM(CASE WHEN bulan ='08' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS AGS,
							CASE WHEN SUM(CASE WHEN bulan ='09' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS SEP,
							CASE WHEN SUM(CASE WHEN bulan ='10' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS OKT,
							CASE WHEN SUM(CASE WHEN bulan ='11' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS NOV,
							CASE WHEN SUM(CASE WHEN bulan ='12' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS DES
							FROM besar_laporan
							WHERE deleted_at IS NULL
							GROUP BY id_besar, tahun
						UNION ALL
						SELECT id_supplier AS id_organisasi, tahun, 'supplier' AS perusahaan,
							CASE WHEN SUM(CASE WHEN bulan ='01' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS JAN,
							CASE WHEN SUM(CASE WHEN bulan ='02' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS FEB,
							CASE WHEN SUM(CASE WHEN bulan ='03' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS MAR,
							CASE WHEN SUM(CASE WHEN bulan ='04' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS APR,
							CASE WHEN SUM(CASE WHEN bulan ='05' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS MEI,
							CASE WHEN SUM(CASE WHEN bulan ='06' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS JUN,
							CASE WHEN SUM(CASE WHEN bulan ='07' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS JUL,
							CASE WHEN SUM(CASE WHEN bulan ='08' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS AGS,
							CASE WHEN SUM(CASE WHEN bulan ='09' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS SEP,
							CASE WHEN SUM(CASE WHEN bulan ='10' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS OKT,
							CASE WHEN SUM(CASE WHEN bulan ='11' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS NOV,
							CASE WHEN SUM(CASE WHEN bulan ='12' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS DES
							FROM supplier_laporan
							WHERE deleted_at IS NULL
							GROUP BY id_supplier, tahun
						UNION ALL
						SELECT id_ukm AS id_organisasi, tahun, 'ukm' AS perusahaan,
							CASE WHEN SUM(CASE WHEN bulan ='01' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS JAN,
							CASE WHEN SUM(CASE WHEN bulan ='02' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS FEB,
							CASE WHEN SUM(CASE WHEN bulan ='03' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS MAR,
							CASE WHEN SUM(CASE WHEN bulan ='04' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS APR,
							CASE WHEN SUM(CASE WHEN bulan ='05' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS MEI,
							CASE WHEN SUM(CASE WHEN bulan ='06' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS JUN,
							CASE WHEN SUM(CASE WHEN bulan ='07' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS JUL,
							CASE WHEN SUM(CASE WHEN bulan ='08' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS AGS,
							CASE WHEN SUM(CASE WHEN bulan ='09' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS SEP,
							CASE WHEN SUM(CASE WHEN bulan ='10' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS OKT,
							CASE WHEN SUM(CASE WHEN bulan ='11' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS NOV,
							CASE WHEN SUM(CASE WHEN bulan ='12' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS DES
							FROM ukm_laporan
							WHERE deleted_at IS NULL
							GROUP BY id_ukm, tahun)la
					JOIN perusahaan_vw pv ON pv.id=la.id_organisasi AND pv.perusahaan=la.perusahaan
					WHERE la.tahun={$tahun}
					GROUP BY id_kota)pvv ON pvv.id_kota=p.id
					WHERE p.id_propinsi=$id_propinsi
			$pagging";
			
		return $this->db->query($sql);
	}

	public function get_kota_detil($id_kota, $tahun, $krit, $off = -1, $limit = 20)
	{
		if($krit=='hcfc'){
			$besar="pembelian_hcfc";
			$supplier="produk ='hcfc'";
			$ukm="0";
		}else{
			$besar="pembelian_preblended";
			$supplier="produk ='preblended'";
			$ukm="pembelian_preblended";
		}
		
		$pagging = '';
		if($off >= 0)
		{
			$pagging = " LIMIT $off, $limit";
		}

		$sql="SELECT pv.id, pv.nama,
						CASE WHEN JAN IS NULL THEN 0.00 ELSE ROUND(JAN,2) END AS JAN,
						CASE WHEN FEB IS NULL THEN 0.00 ELSE ROUND(FEB,2) END AS FEB,
						CASE WHEN MAR IS NULL THEN 0.00 ELSE ROUND(MAR,2) END AS MAR,
						CASE WHEN APR IS NULL THEN 0.00 ELSE ROUND(APR,2) END AS APR,
						CASE WHEN MEI IS NULL THEN 0.00 ELSE ROUND(MEI,2) END AS MEI,
						CASE WHEN JUN IS NULL THEN 0.00 ELSE ROUND(JUN,2) END AS JUN,
						CASE WHEN JUL IS NULL THEN 0.00 ELSE ROUND(JUL,2) END AS JUL,
						CASE WHEN AGS IS NULL THEN 0.00 ELSE ROUND(AGS,2) END AS AGS,
						CASE WHEN SEP IS NULL THEN 0.00 ELSE ROUND(SEP,2) END AS SEP,
						CASE WHEN OKT IS NULL THEN 0.00 ELSE ROUND(OKT,2) END AS OKT,
						CASE WHEN NOV IS NULL THEN 0.00 ELSE ROUND(NOV,2) END AS NOV,
						CASE WHEN DES IS NULL THEN 0.00 ELSE ROUND(DES,2) END AS DES
						FROM perusahaan_vw pv 
						LEFT JOIN (SELECT la.*
							FROM
							(SELECT id_besar AS id_organisasi, tahun, 'besar' AS perusahaan,
							CASE WHEN SUM(CASE WHEN bulan ='01' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS JAN,
							CASE WHEN SUM(CASE WHEN bulan ='02' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS FEB,
							CASE WHEN SUM(CASE WHEN bulan ='03' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS MAR,
							CASE WHEN SUM(CASE WHEN bulan ='04' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS APR,
							CASE WHEN SUM(CASE WHEN bulan ='05' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS MEI,
							CASE WHEN SUM(CASE WHEN bulan ='06' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS JUN,
							CASE WHEN SUM(CASE WHEN bulan ='07' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS JUL,
							CASE WHEN SUM(CASE WHEN bulan ='08' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS AGS,
							CASE WHEN SUM(CASE WHEN bulan ='09' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS SEP,
							CASE WHEN SUM(CASE WHEN bulan ='10' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS OKT,
							CASE WHEN SUM(CASE WHEN bulan ='11' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS NOV,
							CASE WHEN SUM(CASE WHEN bulan ='12' THEN $besar ELSE 0 END)>0 THEN $besar ELSE 0 END AS DES
							FROM besar_laporan
							WHERE deleted_at IS NULL
							GROUP BY id_besar, tahun
						UNION ALL
						SELECT id_supplier AS id_organisasi, tahun, 'supplier' AS perusahaan,
							CASE WHEN SUM(CASE WHEN bulan ='01' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS JAN,
							CASE WHEN SUM(CASE WHEN bulan ='02' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS FEB,
							CASE WHEN SUM(CASE WHEN bulan ='03' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS MAR,
							CASE WHEN SUM(CASE WHEN bulan ='04' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS APR,
							CASE WHEN SUM(CASE WHEN bulan ='05' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS MEI,
							CASE WHEN SUM(CASE WHEN bulan ='06' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS JUN,
							CASE WHEN SUM(CASE WHEN bulan ='07' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS JUL,
							CASE WHEN SUM(CASE WHEN bulan ='08' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS AGS,
							CASE WHEN SUM(CASE WHEN bulan ='09' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS SEP,
							CASE WHEN SUM(CASE WHEN bulan ='10' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS OKT,
							CASE WHEN SUM(CASE WHEN bulan ='11' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS NOV,
							CASE WHEN SUM(CASE WHEN bulan ='12' AND $supplier THEN penjualan ELSE 0 END)>0 THEN penjualan ELSE 0 END AS DES
							FROM supplier_laporan
							WHERE deleted_at IS NULL
							GROUP BY id_supplier, tahun
						UNION ALL
						SELECT id_ukm AS id_organisasi, tahun, 'ukm' AS perusahaan,
							CASE WHEN SUM(CASE WHEN bulan ='01' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS JAN,
							CASE WHEN SUM(CASE WHEN bulan ='02' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS FEB,
							CASE WHEN SUM(CASE WHEN bulan ='03' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS MAR,
							CASE WHEN SUM(CASE WHEN bulan ='04' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS APR,
							CASE WHEN SUM(CASE WHEN bulan ='05' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS MEI,
							CASE WHEN SUM(CASE WHEN bulan ='06' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS JUN,
							CASE WHEN SUM(CASE WHEN bulan ='07' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS JUL,
							CASE WHEN SUM(CASE WHEN bulan ='08' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS AGS,
							CASE WHEN SUM(CASE WHEN bulan ='09' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS SEP,
							CASE WHEN SUM(CASE WHEN bulan ='10' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS OKT,
							CASE WHEN SUM(CASE WHEN bulan ='11' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS NOV,
							CASE WHEN SUM(CASE WHEN bulan ='12' THEN $ukm ELSE 0 END)>0 THEN pembelian_preblended ELSE 0 END AS DES
							FROM ukm_laporan
							WHERE deleted_at IS NULL
							GROUP BY id_ukm, tahun)la
							WHERE la.tahun={$tahun})laa ON laa.id_organisasi=pv.id AND laa.perusahaan=pv.perusahaan
						WHERE pv.id_kota=$id_kota
			$pagging";

		return $this->db->query($sql);
	}
}
/* End of file hcfc_model.php */
/* Location: ./application/modules/rekap/models/hcfc_model.php */