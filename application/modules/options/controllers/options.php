<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Options extends MX_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
	}
	
	
	public function propinsi($selected = '')
	{
		$src = $this->db->order_by('nama')->get('propinsi');
		return options($src, 'id', $selected, 'nama');
	}
		
	public function tahun($selected = '')
	{
		$src = $this->db->order_by('tahun')->get('tahun');
		return options($src, 'tahun', $selected, 'tahun');
	}
	
	public function bulan($selected = '')
	{
		$src = $this->db->order_by('bulan')->get('bulan');
		return options($src, 'bulan', $selected, 'bulan');
	}
	
	public function provinsi($selected = '')
	{
		if(user_session('id_propinsi') != NULL)
		$this->db->where(array('id' => user_session('id_propinsi') == NULL ? '' : user_session('id_propinsi')));
		
		$this->db->where("deleted_at IS NULL");
		$src = $this->db->order_by('id')->get('propinsi');
		return options($src, 'id', $selected, 'nama');
	}
	
	public function kota($selected = '')
	{
		$this->db->where("deleted_at IS NULL");
		$src = $this->db->order_by('nama')->get('kota');
		return options($src, 'id', $selected, 'nama');
	}

	public function kategori($selected = '')
	{
		$this->db->where("deleted_at IS NULL");
		$src = $this->db->order_by('nama')->get('pengaduan_kategori');
		return options($src, 'id', $selected, 'nama');
	}
	
	public function kotaku($selected = '')
	{
		if(user_session('id_kota') != NULL)
		$this->db->where(array('id' => user_session('id_kota') == NULL ? '' : user_session('id_kota')));
		if(user_session('id_propinsi') != NULL)
		$this->db->where(array('id_propinsi' => user_session('id_propinsi') == NULL ? '' : user_session('id_propinsi')));
	
		$this->db->where("deleted_at IS NULL");
		$src = $this->db->order_by('nama')->get('kota');
		return options($src, 'id', $selected, 'nama');
	}
	
	
	public function supplier($selected = '')
	{
		$this->db->where("deleted_at IS NULL");
		$src = $this->db->order_by('nama')->get('supplier');
		return options($src, 'id', $selected, 'nama');
	}
	
	
	public function besar($selected = '')
	{
		if (user_session('grup_pengguna') == 'besar')
			$this->db->where(array('besar.id' => user_session('id_organisasi')));
		
		if (user_session('tingkatan') == '2')
			$this->db->where(array('id_propinsi' => user_session('id_propinsi')));
		
		if (user_session('tingkatan') == '3')
			$this->db->where(array('id_kota' => user_session('id_kota')));
		
		$this->db->select("besar.*", FALSE);
		$this->db->join("kota AS k", "besar.id_kota=k.id", 'left');
		$this->db->where("besar.deleted_at IS NULL");
		$src = $this->db->order_by('besar.nama')->get('besar');
		
		return options($src, 'id', $selected, 'nama');
	}
	
	public function institusi($selected = '')
	{
		if (user_session('grup_pengguna') == 'institusi')
			$this->db->where(array('institusi.id' => user_session('id_organisasi')));

		if (user_session('tingkatan') == '2')
			$this->db->where(array('id_propinsi' => user_session('id_propinsi')));
		
		if (user_session('tingkatan') == '3')
			$this->db->where(array('id_kota' => user_session('id_kota')));
		
		$this->db->select("institusi.*", FALSE);
		$this->db->join("kota AS k", "institusi.id_kota=k.id", 'left');
		$this->db->where("institusi.deleted_at IS NULL");
		$src = $this->db->order_by('institusi.nama')->get('institusi');
		
		return options($src, 'id', $selected, 'nama');
	}
	
	public function bengkel($selected = '')
	{
		if (user_session('grup_pengguna') == 'bengkel')
			$this->db->where(array('bengkel.id' => user_session('id_organisasi')));

		if (user_session('tingkatan') == '2')
			$this->db->where(array('id_propinsi' => user_session('id_propinsi')));
		
		if (user_session('tingkatan') == '3')
			$this->db->where(array('id_kota' => user_session('id_kota')));
		
		$this->db->select("bengkel.*", FALSE);
		$this->db->join("kota AS k", "bengkel.id_kota=k.id", 'left');
		$this->db->where("bengkel.deleted_at IS NULL");
		$src = $this->db->order_by('bengkel.nama')->get('bengkel');
		
		return options($src, 'id', $selected, 'nama');
	}

	public function ukm($selected = '')
	{
		if (user_session('grup_pengguna') == 'ukm')
			$this->db->where(array('ukm.id' => user_session('id_organisasi')));

		if (user_session('tingkatan') == '2')
			$this->db->where(array('id_propinsi' => user_session('id_propinsi')));
		
		if (user_session('tingkatan') == '3')
			$this->db->where(array('id_kota' => user_session('id_kota')));

		$this->db->select("ukm.*", FALSE);
		$this->db->join("kota AS k", "ukm.id_kota=k.id", 'left');
		$this->db->where("ukm.deleted_at IS NULL");
		$src = $this->db->order_by('ukm.nama')->get('ukm');
		
		return options($src, 'id', $selected, 'nama');
	}

	public function supplier_($selected = '')
	{
		if (user_session('grup_pengguna') == 'supplier')
			$this->db->where(array('ukm.id' => user_session('id_organisasi')));

		if (user_session('tingkatan') == '2')
			$this->db->where(array('id_propinsi' => user_session('id_propinsi')));
		
		if (user_session('tingkatan') == '3')
			$this->db->where(array('id_kota' => user_session('id_kota')));

		$this->db->select("supplier.*", FALSE);
		$this->db->join("kota AS k", "supplier.id_kota=k.id", 'left');
		$this->db->where("supplier.deleted_at IS NULL");
		$src = $this->db->order_by('supplier.nama')->get('supplier');
		
		return options($src, 'id', $selected, 'nama');
	}

	public function perusahaan($selected = '')
	{
		$src = $this->db->get('perusahaan_vw');

		return options_special($src, 'id', 'perusahaan' , $selected, 'nama');
	}

	public function customer($selected = '', $selected2 = '')
	{

		$this->db->select("*", FALSE);
		$this->db->where("deleted_at IS NULL AND id_supplier='$selected2'");
		$src = $this->db->order_by('nama')->get('supplier_customer');
		
		return options($src, 'id', $selected, 'nama');
	}	
	
}

/* End of file options.php */
/* Location: ./application/modules/options/controllers/options.php */
