<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Pelaporan Bengkel / Workshop Nasional</h1>
</div>
<div id="main-container" class="container-fluid">
	<div class="clearfix"></div>
	<br>
	<form method="post" action="<?php echo $show_url; ?>">
		<div class="actions">Tahun
			<select class="col-sm-2 col-md-2" name="tahun">
				<?php echo modules::run('options/tahun', $tahun); ?>
			</select>
			<button class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i></button>
			<div class="clearfix"></div>
		</div>
	</form>
	<div id="num-data"><i class="glyphicon glyphicon-stats"></i>&nbsp; <?php echo $total_data; ?> data</div>
	<!-- <?php echo $grid; ?> -->
	<!-- <?php echo $pagelink; ?> -->
	<div class="col-sm-12">
	<div class="table-responsive">
		<table class="table table-profile table-striped table-condensed">
			<thead>
				<tr>
					<th>No.</th>
					<th>Propinsi</th>
					<th>JAN</th>
					<th>FEB</th>
					<th>MAR</th>
					<th>APR</th>
					<th>MEI</th>
					<th>JUN</th>
					<th>JUL</th>
					<th>AGS</th>
					<th>SEP</th>
					<th>OKT</th>
					<th>NOV</th>
					<th>DES</th>
					<th><?php echo $tahun; ?></th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$no=0;
					foreach ($propinsi->result() as $p): 
					$no++;
					$total=($p->JAN+$p->FEB+$p->MAR+$p->APR+$p->MEI+$p->JUN+$p->JUL+$p->AGS+$p->SEP+$p->OKT+$p->NOV+$p->DES)/12;
				?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><a href="<?php echo $this->page->base_url('/profil/'.$p->id.'/'.$tahun); ?>"><?php echo $p->nama; ?></td>
					<td align="center"><?php echo $p->JAN; ?>%</td>
					<td align="center"><?php echo $p->FEB; ?>%</td>
					<td align="center"><?php echo $p->MAR; ?>%</td>
					<td align="center"><?php echo $p->APR; ?>%</td>
					<td align="center"><?php echo $p->MEI; ?>%</td>
					<td align="center"><?php echo $p->JUN; ?>%</td>
					<td align="center"><?php echo $p->JUL; ?>%</td>
					<td align="center"><?php echo $p->AGS; ?>%</td>
					<td align="center"><?php echo $p->SEP; ?>%</td>
					<td align="center"><?php echo $p->OKT; ?>%</td>
					<td align="center"><?php echo $p->NOV; ?>%</td>
					<td align="center"><?php echo $p->DES; ?>%</td>
					<td align="center"><?php echo round($total,2); ?>%</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	</div>
</div>
<!-- <div id="modal-metadata" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Metadata</h4>
			</div>
			<div class="modal-body">
				<table class="table table-striped table-condesed">
					<tbody>
						<tr>
							<td>Nama Bengkel</td>
							<td id="metadata-nama">Memuat data...</td>
						</tr>
						<tr>
							<td>ID Bengkel</td>
							<td id="metadata-id">Memuat data...</td>
						</tr>
						<tr>
							<td>Waktu Input</td>
							<td id="metadata-created-at">Memuat data...</td>
						</tr>
						<tr>
							<td>Pengguna Input</td>
							<td id="metadata-created-by">Memuat data...</td>
						</tr>
						<tr>
							<td>Waktu Terakhir Update</td>
							<td id="metadata-updated-at">Memuat data...</td>
						</tr>
						<tr>
							<td>Pengguna Terakhir Update</td>
							<td id="metadata-updated-by">Memuat data...</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">OKE</button>
			</div>
		</div>
	</div>
</div>-->
<script>
$().ready(function() {
	/*$('a .glyphicon-menu-hamburger').click(function(e) {
		e.preventDefault();
		var id = $(this).parent().attr('data-id');
		
		$('#metadata-nama, #metadata-id, #metadata-created-at, #metadata-created-by, #metadata-updated-at, #metadata-updated-by').text('Memuat data...');
		$('#modal-metadata').modal('show');
		
		$.post (
			'<?php echo site_url('/perusahaan/bengkel/ajax_get'); ?>'
			, { id: id }
			, function(response) {
				$('#metadata-nama').html(response.nama);
				$('#metadata-id').html(response.id);
				$('#metadata-created-at').html(response.created_at);
				$('#metadata-created-by').html(response.pengguna_input);
				$('#metadata-updated-at').html(response.updated_at);
				$('#metadata-updated-by').html(response.pengguna_update);
			}
		);
	});*/
});
</script>
<style type="text/css" media="screen">
	.divkiri{
		float:left;
		width: 80%;
	}
	.divkanan{
		float: right;
		width: 15%;
	}
	.outer {
	    display: table;
	    position: absolute;
	    height: 100%;
	    width: 100%;
	}

	.middle {
	    display: table-cell;
	    vertical-align: middle;
	}

	.inner {
	    margin-left: auto;
	    margin-right: auto; 
	    /*width: /*whatever width you want*/;*/
	}
</style>
	