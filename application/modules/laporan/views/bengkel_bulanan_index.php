<script>
	$(document).ready(function() {
		$('.dp').datepicker({
			format: "yyyy-mm",
			viewMode: "months", 
			minViewMode: "months",
			autoclose: true
		});
	});
</script>

<style>
	#tbl_total td {
		border: none;
	}
</style>

<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Laporan Bulanan Bengkel</h1>
	<div class="col-sm-4 col-xs-12 pull-right" id="top-search">
		<form method="post" action="<?php echo $show_url; ?>">
			<div class="input-group col-md-12">
				<input class="form-control dp" placeholder="Bulan" name="tgl_laporan" value="<?php echo $bulan; ?>" />
				<div class="input-group-btn">
					<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
				</div>
			</div>
		</form>
	</div>
</div>
<div id="main-container" style="margin:auto 12px; padding-top:20px;">
	<div class="col-md-8">
		<?php echo $grid; ?>
	</div>
	<div class="col-md-4" style="border:1px solid lightgrey; border-radius:5px; padding-top:15px; margin:5px auto;">
		<table id="tbl_total" class="table table-non-fluid">
			<tr>
				<td><b>Pemakaian Freon</b></td>
				<td> : </td>
				<td><?php echo number_format($total_freon); ?> Kg</td>
			</tr>
			<tr>
				<td><b>Total Biaya</b></td>
				<td> : </td>
				<td>Rp. <?php echo number_format($total_biaya); ?></td>
			</tr>
		</table>
	</div>
	<div class="clearfix"></div>
	<div align="">
		<?php echo $pagelink; ?>
	</div>
</div>