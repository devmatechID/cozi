<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Laporan Harian Bengkel</h1>
</div>
<div id="main-container" class="container-fluid">
	<form method="post" action="<?php echo $show_url; ?>">
		<div class="actions">
			<div class="btn-group pull-left">
				<a href="<?php echo $add_url; ?>" class="btn btn-success" title="Tambah Data"><i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs">Tambah Data</span></a>
			</div>
			<select class=" required" name="id_bengkel">
				<?php if (user_session('tingkatan') != 4): ?>
				<option value="">- Pilih Bengkel / Workshop -</option>
				<?php endif; ?>
				<?php echo modules::run('options/bengkel', $data->id_bengkel); ?>
			</select>
			<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i></button>	
			<div class="clearfix"></div>
		</div>
	</form>
	<?php echo $grid; ?>
	<?php echo $pagelink; ?>
</div>