<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bengkel_model extends MY_Model {
	
	public $bulan;
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'bengkel_laphar_vw';
		$this->bulan = date("Y-m");
		
		$this->like = array();
		$this->filter = array (
			'id_bengkel' => (user_session('grup_pengguna') == 'bengkel') ? user_session('id_organisasi') : '',
			'id_propinsi' => (user_session('tingkatan') == '2') ? user_session('id_propinsi') : '',
			'idkota' => (user_session('tingkatan') == '3') ? user_session('id_kota'): '',
		);
		
		$this->fields = (object) array (
			'id_bengkel' => '',
			'tgl_laporan' => '',
			'nama' => '',
			'alamat' => '',
			'no_telp' => '',
			'lokasi' => '',
			'no_ac' => '',
			'merk_ac' => '',
			'pk' => '',
			'jenis' => '',
			'ampere' => '',
			'pressure' => '',
			'jenis_servis' => '',
			'tgl_servis_berikutnya' => '',
			'biaya' => '',
		);
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*,  b.nama AS bengkel, c.nama AS pengguna", FALSE);
		$this->db->join("bengkel AS b", "$main_table.id_bengkel = b.id", 'left');
		$this->db->join("pengguna AS c", "$main_table.created_by = c.id", 'left');
		$this->db->order_by('created_at DESC, b.nama');
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}
	
	
	public function getBulanan() {
		$main_table = $this->table;
		$bulan = $this->bulan;
		$this->db->select("$main_table.*,  b.nama AS bengkel, c.nama AS pengguna", FALSE);
		$this->db->join("bengkel AS b", "$main_table.id_bengkel = b.id", 'left');
		$this->db->join("pengguna AS c", "$main_table.created_by = c.id", 'left');
		$this->db->where("tgl_laporan BETWEEN '$bulan-01' AND '$bulan-31'");
		$this->db->order_by("created_at DESC, $main_table.bengkel");
		
		return $this->db->get($main_table);
	}
	
	public function setBulan($q_encoded) {
		$params_json = base64_decode($q_encoded);
		$params_arr = (mb_detect_encoding($params_json) == 'ASCII') ? (array) json_decode($params_json) : array();
		foreach ($params_arr as $key => $val) {
			if($key == 'tgl_laporan') {
				$this->bulan = $val;
			}
		}
	}
	
	public function setLimitBulanan($params) {
		$this->db->limit($params['limit'], $params['offset']);
		$this->db->order_by($this->table.".".$params['item'], $params['order']);
	}
	
	
}
/* End of file bengkel_model.php */
/* Location: ./application/modules/hpmp/models/bengkel_model.php */