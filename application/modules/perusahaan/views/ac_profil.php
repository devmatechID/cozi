
	<script type="text/javascript" src="<?php echo base_url()?>fancyBox/source/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>fancyBox/source/jquery.fancybox.css" media="screen" />
    <style type="text/css" media="screen">
        .foto{
            width: 100%;
            height: 200px;
        }
    </style>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();		
		});
	</script>
    
    <div id="header" class="container-fluid">
    <h1 class="col-md-6"><img src="<?php echo base_url(); ?>img/ac.png">&nbsp; <?php echo $data->nama; ?></h1>
    <div class="col-md-6">
        <div class="btn-group pull-right">
            <a href="<?php echo $form_url; ?>" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs">UBAH</span></a>
            <a href="<?php echo $images_url; ?>" class="btn btn-success"><i class="glyphicon glyphicon-picture"></i> <span class="hidden-xs">UPLOAD FOTO</span></a>
        </div>
    </div>
</div>
<div id="main-container" class="container-fluid">
    <div class="col-sm-3">
        <dl class="profile">
            <dt>Profil</dt>
            <dd><?php echo $data->profil; ?></dd>
        </dl>
        <dl class="profile">
            <dt>Produk</dt>
            <dd><?php echo $data->produk; ?></dd>
        </dl>
        <dl class="profile">
            <dt>Alih Teknologi HPMP</dt>
            <dd><?php echo $data->alih_teknologi; ?></dd>
        </dl>
        <dl class="profile">
            <dt>Nama Kontak</dt>
            <dd><?php echo $data->nama_kontak; ?></dd>
        </dl>
        <dl class="profile">
            <dt>No. Telp.</dt>
            <dd><?php echo $data->no_telp; ?></dd>
        </dl>
        <dl class="profile">
            <dt>Email</dt>
            <dd><?php echo $data->email; ?></dd>
        </dl>
        <div class="separator2"></div>
    </div>
    <div class="col-sm-9">
        <div class="clearfix"></div>
        <?php if ($data->lokasi != NULL && $data->lokasi != ''): ?>
            <div id="ac-map" style="height: 250px;"></div>
        <?php else: ?>
            <div class="blank-map">Belum ada lokasi</div>
        <?php endif; ?>
    </div>
    <div class="col-sm-9">
        <div class="row">
            <br>
        </div>
    </div>
    <div class="col-sm-9">

        <?php if ($images->num_rows() == 0): ?>
            <div class="alert alert-warning">Belum ada foto.</div>
        <?php else: ?>
            <?php foreach ($images->result() as $img): ?>
                <div class="col-sm-4">
                    <a class="fancybox fancybox.image" data-fancybox-group="data" href="<?php echo base_url("/img/ac/{$img->id_ac}/{$img->nama_file}"); ?>">
                        <img src="<?php echo base_url("/img/ac/{$img->id_ac}/{$img->nama_file}"); ?>" class="img-responsive foto">
                        <div class="caption"><?php echo $img->keterangan; ?></div>
                    </a>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
        <div class="clearfix"></div>

    </div>
</div>
<?php if ($data->lokasi != NULL && $data->lokasi != ''): ?>
    <?php $position = location($data->lokasi); ?>
    console.log(location($data->lokasi));

    <script>
        function initMap() {
            var ac = { lat: <?php echo $position['lat'] ?>, lng: <?php echo $position['long'] ?> };
            console.log(ac);
            var map = new google.maps.Map(document.getElementById('ac-map'), { zoom: 15, center: ac });
            var marker = new google.maps.Marker({ position: ac, map: map, icon: '<?php echo base_url('/img/marker.png'); ?>' });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnoCzqdU2bCgUMFH8asKC3YgrlYNY4KKM&callback=initMap"></script>
<?php endif; ?>