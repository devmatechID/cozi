<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Data Manufaktur AC</h1>
	<div class="col-sm-4 col-xs-12 pull-right" id="top-search">
		<form method="post" action="<?php echo $show_url; ?>">
			<div class="input-group col-md-12">
				<input type="text" class="form-control" placeholder="Cari Nama Manufaktur AC..." name="keyword" value="<?php echo $keyword; ?>">
				<div class="input-group-btn">
					<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
				</div>
			</div>
		</form>
	</div>
</div>
<div id="main-container" class="container-fluid">
	<div class="col-sm-12">
		<div id="map-awal-bengkel" style="position: relative; overflow: hidden;height:395px;">
		</div>
	</div>
	<div class="clearfix"></div>
	<br>
	<form method="post" action="<?php echo $show_url; ?>">
		<div class="actions">
			<div class="btn-group pull-left">
				<a href="<?php echo $add_url; ?>" class="btn btn-success" title="Tambah Data"><i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs">Tambah Data</span></a>
			</div>
			<div class="clearfix"></div>
		</div>
	</form>
	<div id="num-data"><i class="glyphicon glyphicon-stats"></i>&nbsp; <?php echo $total_data; ?> data</div>
	<?php echo $grid; ?>
	<?php echo $pagelink; ?>
</div>
<script>

	function initMap() {
		$lat = -2.952997;
		$lng = 117.521710;
		$zoom = 5;
		var uluru = {lat: $lat, lng: $lng};
		var map = new google.maps.Map(document.getElementById('map-awal-bengkel'), { zoom: $zoom,center: uluru});
		var markers = [];
		var pesan = "";
		$.getJSON("<?php echo $this->page->base_url("get_bengkel")?>", {}, function(res){

			if(res.length != 0){
				$.each(res, function (i, result) {
					var lokasi = result.lokasi.split(",");
					// var myLatlng = new google.maps.LatLng(lokasi[0],lokasi[1]);
					//             var marker = new google.maps.Marker({
					//                 position: myLatlng,
					//                 map: map,
					//                 icon: '<?php echo base_url('/img/marker.png'); ?>',
					//                 title: result.nama
					//             });
					var latLng = new google.maps.LatLng(lokasi[0],lokasi[1]);
					var marker = new google.maps.Marker({'position': latLng,icon:'<?php echo base_url('/img/marker.png'); ?>'});

					var contentString = '<div id="content">'+
						'<div id="siteNotice">'+
						'</div>'+
						'<div id="bodyContent">'+
						// '<div class="divkiri">'+
						'<h5>'+result.nama+'</h5>'+
						'<a target="_blank" href="https://maps.google.com/?daddr='+lokasi[0]+','+lokasi[1]+'"><p> NAVIGASI</p></a>'+
						// '<p style="font-size:9px;">'+lokasi[0]+' , '+lokasi[1]+'</p>'+
						// '</div>'+
						// 		'<div class="divkanan">'+
						// 	// 		'<div class="outer">'+
						// 	// '<div class="middle">'+
						// 	// 	'<div class="inner">'+
						// 		'<a target="_blank" href="https://maps.google.com/?daddr='+lokasi[0]+','+lokasi[1]+'"><h3><img src="<?php echo base_url('/img/direction.png'); ?>" alt=""></h3></a>'+
						// // 		'</div>'+
						// // 	'</div>'+
						// // '</div>'+
						// 		'</div>'+
						'</div>'+
						'</div>';


					var infowindow = new google.maps.InfoWindow({
						content: contentString,
					});

					marker.addListener('click', function () {
						infowindow.open(map, marker);
					});

					markers.push(marker);



					// pesan.push(contentString);
				});
				var markerCluster = new MarkerClusterer(map, markers, {imagePath: "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m"});
			}

		});
		// console.log(markers);


	}


</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnoCzqdU2bCgUMFH8asKC3YgrlYNY4KKM&callback=initMap"></script>