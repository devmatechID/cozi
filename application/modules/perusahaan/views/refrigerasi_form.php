<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Form Manufaktur Refrigerasi</h1>
</div>
<div id="main-container" class="container-fluid">
	<?php if ($data->nama != ''): ?>
		<div class="actions">
			<div class="btn-group pull-left">
				<a href="<?php echo $form_url; ?>" class="btn btn-success"><i class="glyphicon glyphicon-file"></i> <span class="hidden-xs">FORM</span></a>
				<a href="<?php echo $images_url; ?>" class="btn btn-default"><i class="glyphicon glyphicon-picture"></i> <span class="hidden-xs">FOTO</span></a>
			</div>
			<div class="clearfix"></div>
		</div>
	<?php endif; ?>
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Nama Manufaktur Refrigerasi</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="nama" placeholder="Nama Manufaktur Refrigerasi" value="<?php echo $data->nama; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Profil Perusahaan</label>
				<div class="col-sm-7">
					<textarea class="form-control" name="profil" placeholder="Profil Perusahaan"><?php echo $data->profil; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Produk</label>
				<div class="col-sm-7">
					<textarea class="form-control" name="produk" placeholder="Produk"><?php echo $data->produk; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Alih Teknologi HPMP</label>
				<div class="col-sm-7">
					<textarea class="form-control" name="alih_teknologi" placeholder="Alih Teknologi HPMP"><?php echo $data->alih_teknologi; ?></textarea>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Nama Kontak</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="nama_kontak" placeholder="Nama Kontak" value="<?php echo $data->nama_kontak; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> No. Telp.</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="no_telp" placeholder="No. Telp." value="<?php echo $data->no_telp; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Email</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo $data->email; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Lokasi</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="lokasi" placeholder="Lokasi" value="<?php echo $data->lokasi; ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="form-submit">
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
	</form>
</div>