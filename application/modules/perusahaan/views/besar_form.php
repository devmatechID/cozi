<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Form Foam - Perusahaan Besar</h1>
</div>
<div id="main-container" class="container-fluid">
	<?php if ($data->nama != ''): ?>
		<div class="actions">
			<div class="btn-group pull-left">
				<a href="<?php echo $form_url; ?>" class="btn btn-success"><i class="glyphicon glyphicon-file"></i> <span class="hidden-xs">FORM</span></a>
				<a href="<?php echo $images_url; ?>" class="btn btn-default"><i class="glyphicon glyphicon-picture"></i> <span class="hidden-xs">FOTO</span></a>
			</div>
			<div class="clearfix"></div>
		</div>
	<?php endif; ?>
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Nama Perusahaan</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="nama" placeholder="Nama Perusahaan" value="<?php echo $data->nama; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Alamat</label>
				<div class="col-sm-7">
					<textarea class="form-control required" name="alamat" placeholder="Alamat"><?php echo $data->alamat; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Kota</label>
				<div class="col-sm-7">
					<select name="id_kota" class="form-control required">
						<option value="">- Pilih Kota -</option>
						<?php echo modules::run('options/kota', $data->id_kota); ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> NPWP</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="npwp" placeholder="NPWP" value="<?php echo $data->npwp; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Tahun Berdiri</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="tahun_berdiri" placeholder="Tahun Berdiri" value="<?php echo $data->tahun_berdiri; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Kategori Produk Foam</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="kategori" placeholder="Kategori Produk Foam" value="<?php echo $data->kategori; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Metode Pembuatan Foam</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="metode" placeholder="Metode Pembuatan Foam" value="<?php echo $data->metode; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Supplier HCFC-141b</label>
				<div class="col-sm-7">
					<select class="form-control required" name="id_supplier">
						<?php if (user_session('tingkatan') != 4): ?>
						<option value="">- Pilih System House -</option>
						<?php endif; ?>
						<?php echo modules::run('options/supplier_', $data->id_supplier); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Nama Penanggung Jawab</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="penanggung_jawab" placeholder="Nama Penanggung Jawab" value="<?php echo $data->penanggung_jawab; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> No. Telp.</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="no_telp" placeholder="No. Telp." value="<?php echo $data->no_telp; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Email</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo $data->email; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Lokasi</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="lokasi" placeholder="Lokasi" value="<?php echo $data->lokasi; ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="form-submit">
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
	</form>
</div>