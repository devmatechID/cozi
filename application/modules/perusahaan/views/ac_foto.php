<style type="text/css" media="screen">
    .foto{
        width: 100%;
        height: 200px;
        /*padding: 0 0 0 !important;*/
    }
    .img-wrap .close {
        position: absolute;
        top: 2px;
        right: 2px;
        z-index: 100;
        /*background-color: #FFF;*/
        /*padding: 5px 2px 2px;*/
        color: #000;
        font-size: 29px;
        font-weight: bold;
        cursor: pointer;
        opacity: .2;
        text-align: center;
        line-height: 10px;
        border-radius: 50%;
    }
    .img-wrap:hover .close {
        opacity: 1;
    }

</style>
<div id="header" class="container-fluid">
    <h1 class="col-sm-6">Foto <?php echo $data->nama; ?></h1>
</div>
<div id="main-container" class="container-fluid">
    <div class="actions">
        <div class="btn-group pull-left">
            <a href="<?php echo $form_url; ?>" class="btn btn-default"><i class="glyphicon glyphicon-file"></i> <span class="hidden-xs">FORM</span></a>
            <a href="<?php echo $images_url; ?>" class="btn btn-success"><i class="glyphicon glyphicon-picture"></i> <span class="hidden-xs">FOTO</span></a>
        </div>
        <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#modal-form"><i class="glyphicon glyphicon-camera"></i> <span class="hidden-xs">UNGGAH FOTO</span></a>
        <div class="clearfix"></div>
    </div>
    <?php if ($images->num_rows() == 0): ?>
        <div class="alert alert-warning">Belum ada foto.</div>
    <?php else: ?>
        <?php foreach ($images->result() as $img): ?>
            <div class="col-md-4 img-wrap img-show<?php echo $img->id?>">
                <span class="close">&times;</span>
                <img src="<?php echo base_url("/img/ac/{$img->id_ac}/{$img->nama_file}"); ?>" class="img-responsive foto" data-id="<?php echo $img->id?>">
                <div class="caption"><?php echo $img->keterangan; ?></div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
    <div class="clearfix"></div>
</div>

<div id="modal-form" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form method="post" action="<?php echo $upload_url; ?>" class="form-horizontal validate-form" enctype="multipart/form-data">
            <input type="hidden" name="id_ac" value="<?php echo $data->ac_id; ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Unggah Foto</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-sm-4">File Foto</label>
                        <div class="col-sm-7">
                            <input type="file" class="required" name="foto">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Keterangan</label>
                        <div class="col-sm-7">
                            <textarea class="form-control" name="keterangan" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-success">Unggah Foto</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $('.img-wrap .close').on('click', function() {
            var id = $(this).closest('.img-wrap').find('img').data('id');
            // alert('remove picture: ' + id);
            var currentLocation = window.location;
            if(confirm("Apakah anda akan menghapus foto ini!!"))
            {
              $.ajax({
                   data : {'id':id},
                   type : "post",
                   url : "<?=$this->page->base_url()?>/hapus_foto",
                   async : false,
                   success: function(result)
                   {
                        alert("Foto Berhasil dihapus");
                        window.location = currentLocation;
                        // $(".img-show"+id).remove();  
                   }
              });
              
            }
        });
    });
</script>