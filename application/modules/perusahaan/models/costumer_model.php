<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Costumer_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'supplier_customer';
		
		$this->like = array($this->table.'.nama');
		$this->filter = array();
		
		$this->fields = (object) array (
			'id_supplier' => '',
			'nama' => '',
			'alamat' => '',
			'no_telp' => '',
		);
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*, b.nama AS supplier");
		$this->db->join("supplier AS b", "$main_table.id_supplier = b.id", 'left');
		$this->db->order_by($this->order);
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}
	
	
}
/* End of file supplier_model.php */
/* Location: ./application/modules/perusahaan/models/supplier_model.php */