<script type="text/javascript">
$().ready(function() {
	get_pengguna();
});
	function get_pengguna()
	{
		var pengguna = $("#grup_pengguna").val();
		var id_organisasi = $("#idorg").val();;
		if(pengguna!='superadmin' && pengguna!='publik' && pengguna!=''){
		$.ajax({
			data: {'pengguna':pengguna},
			dataType : 'json',
			type : 'get',
			url : "<?=$this->page->base_url('get_pengguna')?>",
			success : function(hasil)
			{
				// console.log(hasil);
				$opt = '<option value="">- Pilih '+pengguna+' -</option>';
				$.each(hasil, function(index, data){
					if(data['id']==id_organisasi){
					$opt += '<option value="'+data['id']+'" selected>'+data['nama']+'</option>';
					}else{						
					$opt += '<option value="'+data['id']+'">'+data['nama']+'</option>';
					}

				});


				$("#id_organisasi").html($opt);
				// $("#level").html(pengguna);
				$("#organisasi").show();

			}
		});
		}else{
			$("#id_organisasi").html("");
			$("#organisasi").hide();
		}
	}
</script>
<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Manajemen Pengguna</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<input type="hidden" id="idorg" value="<?php echo $data->id_organisasi; ?>">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>Nama Pengguna</label>
				<div class="col-sm-6">
					<input type="text" class="form-control required" name="nama" placeholder="Nama Pengguna" value="<?php echo $data->nama; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">No HP</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="no_hp" placeholder="No. HP" value="<?php echo $data->no_hp; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">Email</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo $data->email; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>User ID</label>
				<div class="col-sm-6">
					<input type="text" class="form-control required" name="user_id" placeholder="User ID" value="<?php echo $data->user_id; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">
				<?php if($data->user_id == ''){echo '<span class="red">*</span>';}else{echo '';}?>Password</label>
				<div class="col-sm-6">
				<?php if($data->user_id == ''){
					echo '<input type="password" class="form-control required" name="password" placeholder="Password" value="">';
				}else{
					echo '<input type="password" class="form-control" name="password" placeholder="Kosongkan Password jika tidak diubah" value="">';
				}?>
					
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>Grup Pengguna</label>
				<div class="col-sm-6">
					<select name="grup_pengguna" id="grup_pengguna" class="form-control required" onchange="get_pengguna();">

					<option value="">- Pilih Grup Pengguna -</option>
					<option value="superadmin" <?php if($data->grup_pengguna == "superadmin") echo "selected"; ?>>Superadmin</option>
					<option value="pemerintah" <?php if($data->grup_pengguna == "pemerintah") echo "selected"; ?>>Pemerintah</option>
					<option value="besar" <?php if($data->grup_pengguna == "besar") echo "selected"; ?>>Perusahaan Besar</option>
					<option value="balai" <?php if($data->grup_pengguna == "balai") echo "selected"; ?>>Balai</option>
					<option value="ukm" <?php if($data->grup_pengguna == "ukm") echo "selected"; ?>>UKM</option>
					<option value="institusi" <?php if($data->grup_pengguna == "institusi") echo "selected"; ?>>Institusi</option>
					<option value="bengkel" <?php if($data->grup_pengguna == "bengkel") echo "selected"; ?>>Bengkel</option>
					<option value="supplier" <?php if($data->grup_pengguna == "supplier") echo "selected"; ?>>Supplier</option>
					<option value="publik" <?php if($data->grup_pengguna == "publik") echo "selected"; ?>>Publik</option>
					<!-- <?php echo modules::run('options/supplier', $data->id_supplier); ?> -->
					</select>
				</div>
			</div>
			<div class="form-group" id="organisasi">
				<label class="control-label col-sm-2"><span class="red">*</span>Organisasi</span></label>
				<div class="col-sm-6">
					<select name="id_organisasi" class="form-control" id="id_organisasi"> 
						<!-- <?php echo modules::run('options/supplier', $data->id_supplier); ?> -->
					</select>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-12">
		<div class="form-group">
				<label class="control-label col-sm-2"></label>
		<div class="form-submit col-md-8" style="text-align: left !important;" >
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
		</div>
		</div>
	</form>
</div>