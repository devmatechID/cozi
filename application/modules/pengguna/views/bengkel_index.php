<style>
	table {
		margin:auto 15px;
	}
</style>

<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Manajemen Pengguna</h1>
	<div class="col-sm-4 col-xs-12 pull-right" id="top-search">
		<form method="post" action="<?php echo $show_url; ?>">
			<div class="input-group col-md-12">
				<input type="text" class="form-control" placeholder="Cari Nama Pengguna / User ID..." name="keyword" value="<?php echo $keyword; ?>">
				<div class="input-group-btn">
					<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
				</div>
			</div>
		</form>
	</div>
</div>
<div id="main-container" class="container-fluid">
	<form method="post" action="<?php echo $show_url; ?>">
		<div class="actions">
			<div class="btn-group pull-left">
				<a href="<?php echo $add_url; ?>" class="btn btn-success" title="Tambah Data"><i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs">Tambah Data</span></a>
			</div>
			<div class="clearfix"></div>
		</div>
	</form>
	<?php echo $grid; ?>
	<?php echo $pagelink; ?>
</div>