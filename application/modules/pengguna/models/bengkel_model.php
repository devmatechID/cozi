<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bengkel_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'pengguna';
		
		$this->like = array('nama','user_id');
		$this->filter = array();
		
		$this->fields = (object) array (
			'nama' => '',
			'no_hp' => '',
			'email' => '',
			'user_id' => '',
			'grup_pengguna' => '',
			'tingkatan' => '',
			'id_organisasi' => '',
		);
	}
	
	
}
/* End of file pengguna_model.php */
/* Location: ./application/modules/pengguna/models/pengguna_model.php */