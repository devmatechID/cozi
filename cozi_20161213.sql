-- revisi view perusahaan_vw
CREATE OR REPLACE
ALGORITHM=UNDEFINED VIEW `perusahaan_vw` AS 
select `bengkel_vw`.`id` AS `id`,`bengkel_vw`.`nama` AS `nama`,'bengkel' AS `perusahaan`, id_kota, id_propinsi, kota, propinsi
from `bengkel_vw` where isnull(`bengkel_vw`.`deleted_at`) 
union all 
select `ukm_vw`.`id` AS `id`,`ukm_vw`.`nama` AS `nama`,'ukm' AS `perusahaan`, id_kota, id_propinsi, kota, propinsi
from `ukm_vw` where isnull(`ukm_vw`.`deleted_at`) 
union all 
select `institusi_vw`.`id` AS `id`,`institusi_vw`.`nama` AS `nama`,'institusi' AS `perusahaan`, id_kota, id_propinsi, kota, propinsi
from `institusi_vw` where isnull(`institusi_vw`.`deleted_at`) 
union all 
select `supplier_vw`.`id` AS `id`,`supplier_vw`.`nama` AS `nama`,'supplier' AS `perusahaan`, id_kota, id_propinsi, kota, propinsi
from `supplier_vw` where isnull(`supplier_vw`.`deleted_at`) 
union all 
select `besar_vw`.`id` AS `id`,`besar_vw`.`nama` AS `nama`,'besar' AS `perusahaan`, id_kota, id_propinsi, kota, propinsi
from `besar_vw` where isnull(`besar_vw`.`deleted_at`);

