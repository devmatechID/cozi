$().ready(function() {
	
	function check_retype()
	{
		var new_password = $('[name=new_password]').val();
		var retype_password = $('[name=retype_password]').val();
		
		if (new_password != retype_password) {
			alert('Password yang diketikkan ulang tidak sama!');
			return false;
		}
		
		return true;
	}
	
	
	$('form').submit(check_retype);
	
});