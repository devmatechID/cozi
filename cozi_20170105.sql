-- create view pengguna_instansi_vw
CREATE OR REPLACE
 ALGORITHM = UNDEFINED
 VIEW `pengguna_instansi_vw`
 AS SELECT p.*
,CASE WHEN p.grup_pengguna='pemerintah' THEN r.nama
WHEN p.grup_pengguna='bengkel' THEN b.nama
WHEN p.grup_pengguna='ukm' THEN u.nama
WHEN p.grup_pengguna='besar' THEN be.nama
WHEN p.grup_pengguna='supplier' THEN s.nama
WHEN p.grup_pengguna='institusi' THEN i.nama
WHEN p.grup_pengguna='superadmin' THEN 'SUPERADMIN'
ELSE 'PUBLIK' END instansi
FROM `pengguna` p 
LEFT JOIN pemerintah r ON r.id=p.id_organisasi AND p.grup_pengguna='pemerintah' AND r.deleted_at IS NULL
LEFT JOIN bengkel b ON b.id=p.id_organisasi AND p.grup_pengguna='bengkel' AND b.deleted_at IS NULL
LEFT JOIN ukm u ON u.id=p.id_organisasi AND p.grup_pengguna='ukm' AND u.deleted_at IS NULL
LEFT JOIN besar be ON be.id=p.id_organisasi AND p.grup_pengguna='besar' AND be.deleted_at IS NULL
LEFT JOIN supplier s ON s.id=p.id_organisasi AND p.grup_pengguna='supplier' AND s.deleted_at IS NULL
LEFT JOIN institusi i ON i.id=p.id_organisasi AND p.grup_pengguna='institusi' AND i.deleted_at IS NULL;

-- create view bengkel_laphar_vw

CREATE OR REPLACE ALGORITHM=UNDEFINED VIEW `bengkel_laphar_vw` AS
select bl.*, b.id_kota , k.id_propinsi, b.nama bengkel
from 
bengkel_laphar bl
join bengkel b on b.id = bl.id_bengkel
left join kota k on k.id = b.id_kota
left join propinsi p on p.id = k.id_propinsi
WHERE b.deleted_at IS NULL AND bl.deleted_at IS NULL;